import 'dart:math';

class Misc {
  static abbrNum(num number, [int decimals = 0]) {
    if (number == null) {
      return null;
    } // terminate early
    if (number == 0) {
      return '0';
    } // terminate early

    decimals =
        (decimals < 0) ? 0 : decimals; // number of decimal places to show

    var b = number.toStringAsPrecision(2).split("e"), // get power

        k = b.length == 1
            ? 0
            : (min(int.parse(b[1].substring(1)), 14) / 3)
                .floor(), // floor at decimals, ceiling at trillions

        c = k < 1
            ? number.toStringAsFixed(0 + decimals)
            : (number / pow(10, k * 3))
                .toStringAsFixed(1 + decimals), // divide by power

        d = double.parse(c) < 0
            ? c
            : double.parse(c).abs().toString(); // enforce -0 is 0

    // bool allZeros = true;
    // d.split('.').forEach((n) => n != '0' ? allZeros = false : null);

    // if(allZeros) d = d.

    var e = double.parse(d).toStringAsFixed(decimals);
    return e +
        [
          '',
          'k',
          'm',
          'b',
          't',
          'q',
          'Q',
          's',
          'S',
          'o',
          'n',
          'D'
        ][k]; // append power
  }

  static String addCommaToNumber(String number) => number.replaceAllMapped(
      RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))'), (Match m) => '${m[1]},');

  static String addCardinal(int i) => i != null
      ? i % 100 == 11 || i % 100 == 12 || i % 100 == 13
          ? "${i}th"
          : '${i}${[
              "th",
              "st",
              "nd",
              "rd",
              "th",
              "th",
              "th",
              "th",
              "th",
              "th"
            ][i % 10]}'
      : 'N/A';
}
