import 'package:auto_size_text/auto_size_text.dart';
import 'package:dankbank/managers/api_manager.dart';
import 'package:dankbank/misc.dart';
import 'package:dankbank/models/investment.dart';
import 'package:dankbank/models/user.dart';
import 'package:dankbank/pages/stocks_page/stocks_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sparkline/flutter_sparkline.dart';

class BalanceTile extends StatelessWidget {
  final String username;
  final Function onRemoveFriend;

  const BalanceTile({Key key, @required this.username, this.onRemoveFriend})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(8.0),
      child: SizedBox(
        height: 220.0,
        child: Material(
          elevation: 6.0,
          shadowColor: Theme.of(context).accentColor.withOpacity(0.3),
          borderRadius: BorderRadius.circular(4.0),
          color: Theme.of(context).accentColor,
          child: InkWell(
            onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (_) => StocksPage(username: username)),
                ).then(
                  (removedUser) => removedUser != null && removedUser
                      ? onRemoveFriend()
                      : null,
                ),
            child: FutureBuilder(
              future: /* chartMem.runOnce(() =>  */ ApiManager
                  .getUser(username) /* ) */,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done &&
                    snapshot.hasData) {
                  final user = snapshot.data as User;

                  return Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(24.0).copyWith(bottom: 0.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            AutoSizeText(
                              snapshot.data != null
                                  ? 'M¢ ' +
                                      Misc.addCommaToNumber(
                                          user.balance.toStringAsFixed(2))
                                  : '...',
                              maxLines: 1,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                                fontSize: 42.0,
                              ),
                            ),
                            Text(
                              username,
                              style: TextStyle(
                                color: Colors.white,
                                // fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 0.0),
                        child: FutureBuilder(
                          future: ApiManager.getInvestments(user.username),
                          builder: (context, snapshot) {
                            if (snapshot.connectionState ==
                                    ConnectionState.done &&
                                snapshot.hasData) {
                              final investments =
                                  (snapshot.data as List<Investment>);

                              List<double> values = investments
                                  .where((inv) => inv.done)
                                  .map((inv) => inv.profit)
                                  .toList();

                              values = values.reversed.toList();
                              // values = values
                              //     .getRange(
                              //         max(values.length - 20, 0), values.length)
                              //     .toList();

                              if (values.length > 1) {
                                // Done investments
                                return Sparkline(
                                  data: values,
                                  lineColor: Colors.white70,
                                  fillMode: FillMode.below,
                                  fillGradient: LinearGradient(
                                    colors: [
                                      Colors.white70,
                                      Colors.white10,
                                      // Theme.of(context).accentColor
                                    ],
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                  ),
                                );
                              } else {
                                return Padding(
                                  padding: const EdgeInsets.only(bottom: 58.0),
                                  child: Center(
                                      child: Text(
                                    'Not enough data',
                                    style: TextStyle(color: Colors.white70),
                                  )),
                                );
                              }
                            }
                            return Center(child: CircularProgressIndicator());
                          },
                        ),
                      ),
                    ],
                  );
                } else
                  return Center(child: CircularProgressIndicator());
              },
            ),
          ),
        ),
      ),
    );
  }
}
