import 'package:dankbank/managers/api_manager.dart';
import 'package:dankbank/managers/auth_manager.dart';
import 'package:dankbank/pages/friends/widgets/balance_tile.dart';
import 'package:flutter/material.dart';

class FriendsPage extends StatefulWidget {
  const FriendsPage({Key key}) : super(key: key);

  @override
  _FriendsPageState createState() => _FriendsPageState();

  static const String routeName = '/friends_page';
}

class _FriendsPageState extends State<FriendsPage> {
  TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    AuthManager.instance.analytics
        .setCurrentScreen(screenName: '${FriendsPage.routeName}');
  }

  _addFriend(String username) async {
    final user = await ApiManager.getUser(username);
    if (user.username.length > 1) {
      setState(() => AuthManager.instance.addFriend(_controller.text.trim()));
    } else
      _showErrorDialog(username);
  }

  _showErrorDialog(name) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
            title: Text('We couldn\'t find the user \'$name\''),
            content: Text(
                'Make sure the username is correct, that the user has opened an account in the meme market, and that your internet connection is working.'),
            actions: <Widget>[
              FlatButton(
                onPressed: () => Navigator.pop(context),
                child: Text('CANCEL'),
              ),
            ],
          ),
    );
  }

  _showAddFriendDialog() {
    showDialog(
      context: context,
      builder: (context) => Dialog(
            child: Material(
              borderRadius: BorderRadius.circular(8.0),
              clipBehavior: Clip.antiAlias,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Text(
                      'Add a friend',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: 20.0,
                      ),
                    ),
                    SizedBox(height: 8.0),
                    TextField(
                      controller: _controller,
                      decoration: InputDecoration(
                        hintText: 'Friend username (whithout u/)',
                      ),
                    ),
                    SizedBox(height: 24.0),
                    Material(
                      elevation: 6.0,
                      clipBehavior: Clip.antiAlias,
                      borderRadius: BorderRadius.circular(4.0),
                      color: Theme.of(context).accentColor,
                      child: InkWell(
                        onTap: () {
                          if (_controller.text.trim().length > 0) {
                            Navigator.pop(context);

                            AuthManager.instance.analytics.logEvent(
                              name: 'add_friend',
                            );
                            _addFriend(_controller.text.trim());
                          }
                        },
                        child: Padding(
                          padding: EdgeInsets.all(12.0),
                          child: Text(
                            'Add',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            backgroundColor: Colors.transparent,
          ),
    );
  }

  _updateFriends() async => setState(() {});

  @override
  Widget build(BuildContext context) {
    if (AuthManager.instance.getFriends() == null ||
        AuthManager.instance.getFriends().length == 0) {
      return Padding(
        padding: const EdgeInsets.all(32.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Image.asset('res/img/friends.png'),
            Column(
              children: <Widget>[
                Text(
                  'It\'s better with friends',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 24.0,
                  ),
                ),
                SizedBox(height: 16.0),
                Text(
                  'Add people by their Reddit username, but remember that they need to have already opened an account on r/MemeEconomy.',
                  textAlign: TextAlign.center,
                  style: TextStyle(),
                ),
              ],
            ),
            Material(
              elevation: 6.0,
              clipBehavior: Clip.antiAlias,
              borderRadius: BorderRadius.circular(8.0),
              color: Theme.of(context).accentColor,
              child: InkWell(
                onTap: _showAddFriendDialog,
                child: Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Text(
                    'Add a friend',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return Stack(
        fit: StackFit.expand,
        children: <Widget>[
          RefreshIndicator(
            onRefresh: () => _updateFriends(),
            child: CustomScrollView(
              slivers: <Widget>[
                SliverList(
                  delegate: SliverChildBuilderDelegate(
                    (context, i) {
                      return BalanceTile(
                        username: AuthManager.instance.getFriends()[i],
                        onRemoveFriend: _updateFriends,
                      );
                    },
                    childCount: AuthManager.instance.getFriends().length,
                  ),
                ),
                SliverToBoxAdapter(child: SizedBox(height: 64.0)),
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: FloatingActionButton.extended(
                onPressed: _showAddFriendDialog,
                backgroundColor: Colors.black,
                icon: Icon(Icons.person_add, color: Colors.white),
                label: Text('Add a friend'),
              ),
            ),
          ),
        ],
      );
    }
  }
}
