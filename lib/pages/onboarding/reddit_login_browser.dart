import 'package:flutter_inappbrowser/flutter_inappbrowser.dart';

class RedditLoginBrowser extends InAppBrowser {
  final Function(String) onPageChange;
  RedditLoginBrowser(this.onPageChange);

  @override
  void onLoadStart(String url) {
    onPageChange(url);
  }
}