import 'package:dankbank/managers/auth_manager.dart';
import 'package:dankbank/managers/reddit_manager.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class OnboardingPage extends StatefulWidget {
  @override
  _OnboardingPageState createState() => _OnboardingPageState();

  static const String routeName = '/login_page';
}

class _OnboardingPageState extends State<OnboardingPage> {
  TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    AuthManager.instance.analytics
        .setCurrentScreen(screenName: '${OnboardingPage.routeName}');
  }

  _login() {
    RedditManager.instance.loginUser(context);

    /* showDialog(
      context: context,
      builder: (context) => AlertDialog(
            content: TextField(
              controller: _controller,
              decoration: InputDecoration(
                hintText: 'Your username (whithout u/)',
              ),
            ),
            actions: <Widget>[
              FlatButton(
                onPressed: () => Navigator.pop(context),
                child: Text('CANCEL'),
              ),
              FlatButton(
                onPressed: () {
                  if (_controller.text.trim().length > 0) {
                    Navigator.pop(context);

                    AuthManager.instance.setUsername(_controller.text.trim());
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(builder: (_) => HomePage()),
                    );
                  }
                },
                child: Text('CONTINUE'),
              ),
            ],
          ),
    );
   */
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Image.asset('res/buildings.jpg', fit: BoxFit.cover),
          Container(color: Colors.black38),
          Padding(
            padding: EdgeInsets.all(32.0),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: 2.0,
                      color: Colors.white,
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(
                      Icons.account_balance,
                      size: 18.0,
                      color: Colors.white,
                    ),
                  ),
                ),
                SizedBox(height: 32.0),
                Text(
                  'Best digital meme investing platform',
                  style: TextStyle(
                    fontSize: 36.0,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 48.0),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Expanded(
                      child: Material(
                        elevation: 6.0,
                        shadowColor: Colors.black26,
                        shape: StadiumBorder(),
                        clipBehavior: Clip.antiAlias,
                        color: Colors.transparent,
                        child: Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [
                                Color(0xff0A6FFF),
                                Color(0xff7F3EFF),
                              ],
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight,
                            ),
                          ),
                          child: Material(
                            color: Colors.transparent,
                            child: InkWell(
                              onTap: () => _login(),
                              child: Padding(
                                padding: EdgeInsets.all(16.0),
                                child: Text(
                                  'Login with Reddit',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 8.0),
                FlatButton(
                  onPressed: () => launch(
                      r'https://2b77b620-1309-4ef1-bf94-40f95181ed1a.htmlpasta.com'),
                  child: Text(
                    'By using this app you accept the Terms and Conditions and Privacy Policy',
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white70, fontSize: 10.0),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
