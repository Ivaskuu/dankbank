import 'package:async/async.dart';
import 'package:dankbank/managers/api_manager.dart';
import 'package:dankbank/managers/auth_manager.dart';
import 'package:dankbank/managers/messages_manager.dart';
import 'package:dankbank/managers/user_manager.dart';
import 'package:dankbank/models/message.dart';
import 'package:dankbank/models/user.dart';
import 'package:dankbank/pages/firms/messages/widgets/chat_textfield.dart';
import 'package:dankbank/pages/firms/messages/widgets/left_bubble.dart';
import 'package:dankbank/pages/firms/messages/widgets/right_bubble.dart';
import 'package:firestore_ui/animated_firestore_list.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class MessagesPage extends StatefulWidget {
  final String firm;
  final String name;
  final int firmId;

  const MessagesPage({
    Key key,
    @required this.firm,
    @required this.name,
    @required this.firmId,
  }) : super(key: key);

  @override
  _MessagesPageState createState() => _MessagesPageState();

  static const String routeName = '/firm_messages_page';
}

class _MessagesPageState extends State<MessagesPage> {
  AsyncMemoizer firmUsersMem = AsyncMemoizer();
  AsyncMemoizer chatUsersMem = AsyncMemoizer();

  _buildMessage(snapshot) {
    final msg = Message.fromDocumentSnapshot(snapshot);

    if (msg.from == widget.name) return RightBubble(msg, widget.firm);
    return LeftBubble(msg);
  }

  @override
  void initState() {
    super.initState();

    AuthManager.instance.analytics
        .setCurrentScreen(screenName: '${MessagesPage.routeName}');
  }

  Future<List<User>> getChatUsers(List<User> users) async {
    List<User> chatUsers = [];

    for (var user in users)
      if (await UserManager.instance.checkUserDownloadedApp(user.username))
        chatUsers.add(user);

    return chatUsers;
  }

  _showMembersDialog({
    @required int firmId,
    @required List<User> firmUsers,
    @required List<User> chatUsers,
  }) {
    showDialog(
      context: context,
      builder: (context) => Dialog(
            child: MembersDialog(
              firmName: widget.firm,
              firmUsers: firmUsers,
              chatUsers: chatUsers,
            ),
            backgroundColor: Colors.transparent,
          ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: FutureBuilder(
          future: firmUsersMem
              .runOnce(() => ApiManager.getFirmUsers(widget.firmId)),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done &&
                snapshot.hasData) {
              final List<User> firmUsers = snapshot.data as List<User>;

              return FutureBuilder(
                future: chatUsersMem.runOnce(() => getChatUsers(firmUsers)),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done &&
                      snapshot.hasData) {
                    List<User> chatUsers = snapshot.data as List<User>;

                    return Row(
                      children: <Widget>[
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                widget.firm,
                                style: TextStyle(
                                  fontSize: 20.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Text(
                                '${chatUsers.length} users in this chat',
                                style: TextStyle(
                                  fontSize: 10.0,
                                  color: Theme.of(context).accentColor,
                                  // fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                        ),
                        IconButton(
                          onPressed: () => _showMembersDialog(
                                firmId: widget.firmId,
                                chatUsers: chatUsers,
                                firmUsers: firmUsers,
                              ),
                          icon: Icon(Icons.people),
                        ),
                      ],
                    );
                  } else
                    return Text(
                      widget.firm,
                      style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                      ),
                    );
                },
              );
            } else
              return Text(
                widget.firm,
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
              );
          },
        ),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: FirestoreAnimatedList(
              query: MessagesManager.instance
                  .getMessages(widget.firm)
                  .orderBy('timestamp', descending: true)
                  .snapshots(),
              reverse: true,
              itemBuilder: (_, snapshot, __, ___) => _buildMessage(snapshot),
              emptyChild: Center(
                child: Container(
                  margin: EdgeInsets.all(16.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text('👋', style: TextStyle(fontSize: 36.0)),
                      SizedBox(height: 32.0),
                      Text(
                        'It\'s empty here! Be the first to say hello',
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(height: 4.0),
                      Text(
                        'Remember this app is still in beta. Only members that use the app will see the messages.',
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Divider(),
          ChatTextfield(
            firm: widget.firm,
            name: widget.name,
            firmId: widget.firmId,
          ),
        ],
      ),
    );
  }
}

class MembersDialog extends StatefulWidget {
  final String firmName;
  final List firmUsers;
  final List chatUsers;

  const MembersDialog({
    Key key,
    @required this.firmName,
    @required this.firmUsers,
    @required this.chatUsers,
  }) : super(key: key);

  @override
  _MembersDialogState createState() => _MembersDialogState();
}

class _MembersDialogState extends State<MembersDialog> {
  List users;
  @override
  void initState() {
    super.initState();
    users = List.from(widget.firmUsers)
      ..sort((a, b) => widget.chatUsers.contains(b) ? 1 : 0);
  }

  _inviteUser(String name) {
    final subject = "You\'re invited to join DankBank";
    final body =
        "Hey there! We are using the DankBank app on Android to chat between us members of '${widget.firmName}', keep track of our investments, receive notifications when investments matures, charts, and much more!\n\nYou are also invited to join us and take the Meme Investing to another level!";

    final url =
        "https://www.reddit.com/message/compose?to=$name&subject=$subject&message=$body";

    launch(Uri.encodeFull(url));
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.circular(8.0),
      clipBehavior: Clip.antiAlias,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text(
              'Firm members',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.black,
                fontSize: 20.0,
              ),
            ),
            SizedBox(height: 24.0),
            SizedBox(
              height: 300,
              child: ListView.builder(
                itemCount: widget.firmUsers.length,
                itemBuilder: (context, i) => ListTile(
                      title: Text(
                        users[i].username,
                        style: TextStyle(
                          color: widget.chatUsers.contains(users[i])
                              ? Theme.of(context).accentColor
                              : Colors.black,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      trailing: widget.chatUsers.contains(users[i])
                          ? null
                          : Material(
                              elevation: 6.0,
                              clipBehavior: Clip.antiAlias,
                              borderRadius: BorderRadius.circular(4.0),
                              color: Theme.of(context).accentColor,
                              child: InkWell(
                                onTap: () => _inviteUser(users[i].username),
                                child: Padding(
                                  padding: EdgeInsets.all(8.0),
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Icon(
                                        Icons.person_add,
                                        color: Colors.white,
                                        size: 16.0,
                                      ),
                                      SizedBox(width: 4.0),
                                      Text(
                                        'Invite',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                    ),
              ),
            ),
            SizedBox(height: 8.0),
            Material(
              clipBehavior: Clip.antiAlias,
              borderRadius: BorderRadius.circular(4.0),
              color: Colors.black12,
              child: InkWell(
                onTap: () => Navigator.pop(context),
                child: Padding(
                  padding: EdgeInsets.all(12.0),
                  child: Text(
                    'Close',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.black,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
