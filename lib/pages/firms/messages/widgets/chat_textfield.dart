import 'package:dankbank/managers/auth_manager.dart';
import 'package:dankbank/managers/messages_manager.dart';
import 'package:dankbank/models/message.dart';
import 'package:flutter/material.dart';

class ChatTextfield extends StatefulWidget {
  final String firm;
  final String name;
  final int firmId;

  const ChatTextfield({
    Key key,
    @required this.firm,
    @required this.name,
    @required this.firmId,
  }) : super(key: key);

  @override
  _ChatTextfieldState createState() => _ChatTextfieldState();
}

class _ChatTextfieldState extends State<ChatTextfield> {
  final TextEditingController controller = TextEditingController();
  bool canSend = false;

  _updateCanSend(bool val) {
    if (canSend != val) setState(() => canSend = val);
  }

  _sendMessage() {
    final text = controller.text.trim();
    if (text.length > 0) {
      MessagesManager.instance.sendMessage(
        Message(
          content: text,
          from: widget.name,
        ),
        widget.firm,
        widget.firmId,
      );
      AuthManager.instance.analytics.logEvent(name: 'send_message');
      controller.clear();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16.0, vertical: 4.0)
          .copyWith(bottom: 10.0, right: 12.0),
      child: Row(
        children: <Widget>[
          // IconButton(
          //   onPressed: null,
          //   icon: Icon(Icons.image),
          // ),
          // SizedBox(width: 16.0),
          Expanded(
            child: TextField(
              onChanged: (text) => text.trim().length > 0
                  ? _updateCanSend(true)
                  : _updateCanSend(false),
              controller: controller,
              textCapitalization: TextCapitalization.sentences,
              decoration: InputDecoration.collapsed(
                hintText: 'Type a message...',
              ),
            ),
          ),
          IconButton(
            onPressed: _sendMessage,
            color: Theme.of(context).accentColor,
            icon: Icon(Icons.send),
          ),
        ],
      ),
    );
  }
}
