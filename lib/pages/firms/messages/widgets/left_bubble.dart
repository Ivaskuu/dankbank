import 'package:dankbank/models/message.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_linkify/flutter_linkify.dart';

import 'package:timeago/timeago.dart' as timeago;
import 'package:url_launcher/url_launcher.dart';

class LeftBubble extends StatelessWidget {
  final Message message;
  const LeftBubble(this.message, {Key key}) : super(key: key);

  _showOptionsDialog(context) {
    showDialog(
      context: context,
      builder: (context) => Dialog(
            child: Material(
              borderRadius: BorderRadius.circular(8.0),
              clipBehavior: Clip.antiAlias,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Text(
                      'Message options',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: 20.0,
                      ),
                    ),
                    SizedBox(height: 4.0),
                    ListTile(
                      onTap: () {
                        Clipboard.setData(ClipboardData(text: message.content));
                        Navigator.pop(context);
                      },
                      leading: Icon(Icons.content_copy),
                      title: Text('Copy'),
                    ),
                    SizedBox(height: 16.0),
                    Material(
                      clipBehavior: Clip.antiAlias,
                      borderRadius: BorderRadius.circular(4.0),
                      color: Colors.black12,
                      child: InkWell(
                        onTap: () => Navigator.pop(context),
                        child: Padding(
                          padding: EdgeInsets.all(12.0),
                          child: Text(
                            'Close',
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.black),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            backgroundColor: Colors.transparent,
          ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 6.0, horizontal: 8.0)
          .copyWith(right: 64.0),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Flexible(
            child: InkWell(
              onTap: () => _showOptionsDialog(context),
              child: Material(
                clipBehavior: Clip.antiAlias,
                borderRadius: BorderRadius.circular(8.0),
                elevation: 8.0,
                shadowColor: Colors.black38,
                color: Colors.white,
                child: Container(
                  margin: EdgeInsets.all(16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Linkify(
                        onOpen: (link) => launch(link.url),
                        text: message.content,
                        style: TextStyle(color: Colors.black),
                        humanize: true,
                      ),
                      SizedBox(height: 2.0),
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            message.from,
                            style: TextStyle(
                              color: Colors.black45,
                              fontSize: 10.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(width: 4.0),
                          Text(
                            timeago.format(message.timestamp.toDate()),
                            style: TextStyle(
                              color: Colors.black45,
                              fontSize: 10.0,
                              // fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
