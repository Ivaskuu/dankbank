import 'package:dankbank/managers/messages_manager.dart';
import 'package:dankbank/models/message.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:url_launcher/url_launcher.dart';

class RightBubble extends StatelessWidget {
  final Message message;
  final String firmName;
  const RightBubble(this.message, this.firmName, {Key key}) : super(key: key);

  _showOptionsDialog(context) {
    showDialog(
      context: context,
      builder: (context) => Dialog(
            child: Material(
              borderRadius: BorderRadius.circular(8.0),
              clipBehavior: Clip.antiAlias,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Text(
                      'Message options',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: 20.0,
                      ),
                    ),
                    SizedBox(height: 4.0),
                    ListTile(
                      onTap: () {
                        Clipboard.setData(ClipboardData(text: message.content));
                        Navigator.pop(context);
                      },
                      leading: Icon(Icons.content_copy),
                      title: Text('Copy'),
                    ),
                    ListTile(
                      onTap: () {
                        _showLoadingDialog(context);
                        MessagesManager.instance
                            .deleteMessage(message, firmName)
                            .then((_) {
                          Navigator.pop(context);
                          Navigator.pop(context);
                        });
                      },
                      leading: Icon(Icons.delete),
                      title: Text('Delete'),
                    ),
                    SizedBox(height: 24.0),
                    Material(
                      clipBehavior: Clip.antiAlias,
                      borderRadius: BorderRadius.circular(4.0),
                      color: Colors.black12,
                      child: InkWell(
                        onTap: () => Navigator.pop(context),
                        child: Padding(
                          padding: EdgeInsets.all(12.0),
                          child: Text(
                            'Close',
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.black),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            backgroundColor: Colors.transparent,
          ),
    );
  }

  _showLoadingDialog(context) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => AlertDialog(
            content: Row(
              children: <Widget>[
                CircularProgressIndicator(),
                SizedBox(width: 8.0),
                Expanded(child: Text('Please wait...')),
              ],
            ),
          ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 6.0, horizontal: 8.0)
          .copyWith(left: 64.0),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Flexible(
            child: InkWell(
              onTap: () => _showOptionsDialog(context),
              child: Material(
                clipBehavior: Clip.antiAlias,
                borderRadius: BorderRadius.circular(8.0),
                elevation: 8.0,
                shadowColor: Colors.black38,
                color: Theme.of(context).accentColor,
                child: Container(
                  margin: EdgeInsets.all(16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Linkify(
                        onOpen: (link) => launch(link.url),
                        text: message.content,
                        style: TextStyle(color: Colors.white),
                        humanize: true,
                      ),
                      SizedBox(width: 4.0),
                      Text(
                        timeago.format(message.timestamp.toDate()),
                        style: TextStyle(
                          color: Colors.white70,
                          fontSize: 10.0,
                          // fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
