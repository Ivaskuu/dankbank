import 'package:async/async.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:dankbank/managers/api_manager.dart';
import 'package:dankbank/managers/auth_manager.dart';
import 'package:dankbank/managers/reddit_manager.dart';
import 'package:dankbank/misc.dart';
import 'package:dankbank/models/firm.dart';
import 'package:dankbank/pages/splash_screen.dart';
import 'package:flutter/material.dart';

class FirmsListPage extends StatefulWidget {
  final String firmId;
  const FirmsListPage({Key key, this.firmId}) : super(key: key);

  @override
  _FirmsListPageState createState() => _FirmsListPageState();

  static const String routeName = '/firms_list_page';
}

class _FirmsListPageState extends State<FirmsListPage> {
  AsyncMemoizer firmsMem = AsyncMemoizer();

  @override
  void initState() {
    super.initState();

    AuthManager.instance.analytics
        .setCurrentScreen(screenName: '${FirmsListPage.routeName}');
  }

  _showJoinFirmDialog() {
    showDialog(
      context: context,
      builder: (context) => Dialog(
            child: JoinFirmDialog(onJoinPress: _onJoinPress),
            backgroundColor: Colors.transparent,
          ),
    );
  }

  _onJoinPress(String name) async {
    _showLoadingDialog();
    // AuthManager.instance.analytics.logEvent(name: 'join_firm');

    // await RedditManager.instance.execCommand('!joinfirm $name');
    // await Future.delayed(Duration(seconds: 5));

    // Navigator.pop(context);
    // AuthManager.instance.localUser = null;

    // Navigator.pushReplacement(
    //     context, MaterialPageRoute(builder: (_) => SplashScreen()));
  }

  _showLoadingDialog() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => AlertDialog(
            content: Row(
              children: <Widget>[
                CircularProgressIndicator(),
                SizedBox(width: 12.0),
                Expanded(child: Text('Please wait...')),
              ],
            ),
          ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Text('Firms list'),
        actions: <Widget>[
          widget.firmId == null
              ? IconButton(
                  onPressed: _showJoinFirmDialog,
                  icon: Icon(Icons.search),
                )
              : Container(),
        ],
      ),
      body: FutureBuilder(
        future: firmsMem.runOnce(() => ApiManager.getFirmsList()),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done &&
              snapshot.hasData) {
            final List<Firm> firms = snapshot.data;

            return ListView.builder(
              itemCount: firms.length,
              itemBuilder: (context, i) {
                bool isThisFirm =
                    widget.firmId != null && firms[i].id == widget.firmId;

                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Opacity(
                    opacity:
                        widget.firmId == null && firms[i].private ? 0.5 : 1.0,
                    child: Material(
                      color: isThisFirm
                          ? Theme.of(context).accentColor
                          : Colors.white,
                      elevation: 6.0,
                      shadowColor: Colors.black38,
                      borderRadius: BorderRadius.circular(8.0),
                      clipBehavior: Clip.antiAlias,
                      child: InkWell(
                        onTap: () => widget.firmId == null && !firms[i].private
                            ? _onJoinPress(firms[i].name)
                            : null,
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Row(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(8.0)
                                    .copyWith(right: 24.0),
                                child: Text(
                                  '${Misc.addCardinal(i + 1)}',
                                  style: TextStyle(
                                    color: isThisFirm
                                        ? Colors.white
                                        : Colors.black,
                                    fontSize: 18.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              Flexible(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      '${firms[i].size} members - ${firms[i].private ? 'Private' : 'Public'}',
                                      style: TextStyle(
                                        color: isThisFirm
                                            ? Colors.white
                                            : Theme.of(context).accentColor,
                                        fontSize: 10.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    SizedBox(height: 4.0),
                                    AutoSizeText(
                                      firms[i].name,
                                      maxLines: 1,
                                      style: TextStyle(
                                        color: isThisFirm
                                            ? Colors.white
                                            : Colors.black,
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    Text(
                                      'F¢ ${Misc.addCommaToNumber(firms[i].balance.toStringAsFixed(2))}',
                                      style: TextStyle(
                                        color: isThisFirm
                                            ? Colors.white70
                                            : Colors.black45,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                );
              },
            );
          } else
            return Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}

class JoinFirmDialog extends StatefulWidget {
  final Function(String) onJoinPress;
  const JoinFirmDialog({Key key, this.onJoinPress}) : super(key: key);

  @override
  _JoinFirmDialogState createState() => _JoinFirmDialogState();
}

class _JoinFirmDialogState extends State<JoinFirmDialog> {
  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.circular(8.0),
      clipBehavior: Clip.antiAlias,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text(
              'Join a firm',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.black,
                fontSize: 20.0,
              ),
            ),
            SizedBox(height: 12.0),
            TextField(
              controller: controller,
              decoration: InputDecoration(
                hintText: 'Firm\'s name',
              ),
            ),
            SizedBox(height: 24.0),
            Material(
              elevation: 6.0,
              clipBehavior: Clip.antiAlias,
              borderRadius: BorderRadius.circular(4.0),
              color: Theme.of(context).accentColor,
              child: InkWell(
                onTap: () {
                  Navigator.pop(context);
                  widget.onJoinPress(controller.text.trim());
                },
                child: Padding(
                  padding: EdgeInsets.all(12.0),
                  child: Text(
                    'Join firm',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(height: 8.0),
            Material(
              clipBehavior: Clip.antiAlias,
              borderRadius: BorderRadius.circular(4.0),
              color: Colors.black12,
              child: InkWell(
                onTap: () => Navigator.pop(context),
                child: Padding(
                  padding: EdgeInsets.all(12.0),
                  child: Text(
                    'Cancel',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.black,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
