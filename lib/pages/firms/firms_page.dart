import 'dart:async';
import 'dart:math';

import 'package:async/async.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:dankbank/managers/api_manager.dart';
import 'package:dankbank/managers/auth_manager.dart';
import 'package:dankbank/managers/reddit_manager.dart';
import 'package:dankbank/managers/user_manager.dart';
import 'package:dankbank/misc.dart';
import 'package:dankbank/models/firm.dart';
import 'package:dankbank/models/user.dart';
import 'package:dankbank/pages/firms/firms_list_page.dart';
import 'package:dankbank/pages/firms/members_page.dart';
import 'package:dankbank/pages/firms/messages/messages_page.dart';
import 'package:dankbank/pages/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class FirmsPage extends StatefulWidget {
  @override
  _FirmsPageState createState() => _FirmsPageState();

  static const String routeName = '/firms_page';
}

class _FirmsPageState extends State<FirmsPage> {
  final AsyncMemoizer userMem = AsyncMemoizer();
  final AsyncMemoizer usefulnessMem = AsyncMemoizer();
  final AsyncMemoizer firmRankMem = AsyncMemoizer();

  AsyncMemoizer firmMem = AsyncMemoizer();

  Map<User, double> usefulness = {};
  List<Map<User, double>> usersPerc;

  @override
  void initState() {
    super.initState();
    AuthManager.instance.analytics
        .setCurrentScreen(screenName: '${FirmsPage.routeName}');
  }

  Future computeUsefulness(int firmId) async {
    print('Computing usefulness');

    final firm = await ApiManager.getFirm(firmId);
    final users = await ApiManager.getFirmUsers(firmId);

    for (var user in users) {
      getImportance(user, firm);
    }

    Timer t;
    t = Timer.periodic(Duration(milliseconds: 100), (_) {
      if (!mounted) t.cancel();

      if (usefulness.length == users.length) {
        t.cancel();

        final double total = usefulness.values.reduce((a, b) => a + b);
        final Map<User, double> perc = usefulness.map<User, double>(
            (key, val) => MapEntry(key, (val / total) * 100));

        usersPerc = [];
        perc.forEach((key, val) => usersPerc.add({key: val}));

        usersPerc.sort((a, b) => b.values.first.compareTo(a.values.first));

        final otherPercs =
            usersPerc.where((val) => val.values.first < 10).toList();
        usersPerc.removeWhere((val) => val.values.first < 10);

        setState(
          () => otherPercs.length > 0
              ? usersPerc.add(otherPercs.reduce((a, b) =>
                  {User(username: 'Other'): a.values.first + b.values.first}))
              : null,
        );
      }
    });
  }

  getImportance(User user, Firm firm) async {
    DateTime lastFriday = getLastFriday();
    print(lastFriday);

    // final lastPayout
    final investments = await ApiManager.getInvestments(user.username);
    if (investments != null && investments.length > 0) {
      investments.removeWhere(
        (inv) =>
            !inv.done ||
            DateTime.fromMillisecondsSinceEpoch(inv.time * 1000)
                .isBefore(lastFriday),
      );

      if (investments.length > 0) {
        int fetched = 0;
        double total = 0;

        for (var inv in investments) {
          RedditManager.instance
              .parseFirmProfitBotComment(inv.responseId, firm.name)
              .then((val) {
            total += (val ?? 0);
            fetched++;
          });
        }

        Timer t;
        t = Timer.periodic(Duration(milliseconds: 100), (_) {
          if (fetched == investments.length) {
            usefulness[user] = total;
            t.cancel();
          }
        });
      } else {
        print('else 1');
        usefulness[user] = 0;
      }
    } else {
      print('else 2');
      usefulness[user] = 0;
    }
  }

  _buildGridTile(IconData icon, String title, String content,
      {List<FirmRole> permissions, FirmRole userRole, Function onTap}) {
    bool hasPermission = permissions == null ||
        permissions.length == 0 ||
        permissions.contains(userRole);

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: InkWell(
        onTap: hasPermission ? onTap : null,
        child: Opacity(
          opacity: hasPermission ? 1 : 0.5,
          child: Material(
            color: Colors.white,
            elevation: 6.0,
            borderRadius: BorderRadius.circular(8.0),
            clipBehavior: Clip.antiAlias,
            shadowColor: Colors.black38,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      CircleAvatar(
                        backgroundColor: Color(0x11000000),
                        child: Icon(icon, color: Colors.black),
                      ),
                      onTap != null
                          ? Icon(Icons.launch, size: 16.0)
                          : Container(),
                    ],
                  ),
                  ListTile(
                    contentPadding: EdgeInsets.all(0.0),
                    title: Text(title),
                    subtitle: Text(
                      content,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 22.0,
                        color: Colors.black,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  DateTime getLastFriday() {
    // Find the last friday at 5:00pm ET
    final etTimeNow = DateTime.now().toUtc().subtract(Duration(hours: 4));
    DateTime lastFriday = etTimeNow;

    if (lastFriday.weekday == 5) {
      if (lastFriday.hour >= 17 + 4) {
        // New week

        lastFriday = DateTime.utc(
            lastFriday.year, lastFriday.month, lastFriday.day, 17 + 4);
        return lastFriday;
      } else {
        // Still last week

        lastFriday = DateTime.utc(
            lastFriday.year, lastFriday.month, lastFriday.day - 7, 17 + 4);
        return lastFriday;
      }
    } else {
      while (lastFriday.weekday != 5) {
        lastFriday = lastFriday.subtract(Duration(days: 1));
      }

      lastFriday = DateTime.utc(
          lastFriday.year, lastFriday.month, lastFriday.day, 17 + 4);
      return lastFriday;
    }
  }

  getPosInFirm(String name) {
    try {
      final membersList = usefulness.entries.toList();
      membersList.sort((a, b) => b.value.compareTo(a.value));

      int pos = membersList
              .indexOf(membersList.firstWhere((m) => m.key.username == name)) +
          1;
      return pos != 0 ? pos : null;
    } catch (ex) {
      return null;
    }
  }

  double calculateNextPayout(Firm firm, User user) {
    final toSplit = (firm.balance * 0.9) / 2;
    final members = usefulness.entries.toList();

    double payout;

    int chiefs = members
        .where((m) =>
            m.key.firmRole == FirmRole.CEO ||
            m.key.firmRole == FirmRole.CFO ||
            m.key.firmRole == FirmRole.COO)
        .length;
    int execs =
        members.where((m) => m.key.firmRole == FirmRole.Executive).length;
    int assocs =
        members.where((m) => m.key.firmRole == FirmRole.Associate).length;
    int fts =
        members.where((m) => m.key.firmRole == FirmRole.Floor_Trader).length;

    if (user.firmRole == FirmRole.CEO ||
        user.firmRole == FirmRole.CFO ||
        user.firmRole == FirmRole.COO) {
      payout = (toSplit * 0.3) / chiefs;
    } else if (user.firmRole == FirmRole.Executive) {
      payout = (toSplit * 0.28) / execs;
    } else if (user.firmRole == FirmRole.Associate) {
      payout = (toSplit * 0.21) / assocs;
    } else {
      payout = (toSplit * 0.21) / fts;
    }

    return payout;
  }

  _showTaxDialog(int tax) {
    showDialog(
      context: context,
      builder: (context) => Dialog(
            child: TaxDialog(currentVal: tax, onNewTax: _onNewTax),
            backgroundColor: Colors.transparent,
          ),
    );
  }

  _onNewTax(int newTax) async {
    _showLoadingDialog();
    AuthManager.instance.analytics.logEvent(name: 'set_tax');

    await RedditManager.instance.postReply(
        '!tax $newTax', await RedditManager.instance.getBotCommentId());
    await Future.delayed(Duration(seconds: 10));

    Navigator.pop(context);
    setState(() {
      firmMem = AsyncMemoizer();
    });
  }

  _showCreateFirmDialog() {
    showDialog(
      context: context,
      builder: (context) => Dialog(
            child: CreateFirmDialog(onCreatePress: _onCreatePress),
            backgroundColor: Colors.transparent,
          ),
    );
  }

  _onCreatePress(String name) async {
    _showLoadingDialog();
    AuthManager.instance.analytics.logEvent(name: 'create_firm');

    await RedditManager.instance.postReply(
        '!createfirm $name', await RedditManager.instance.getBotCommentId());

    await Future.delayed(Duration(seconds: 10));

    Navigator.pop(context);
    AuthManager.instance.localUser = null;
    firmMem = AsyncMemoizer();

    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (_) => SplashScreen()));
  }

  _showVisibilityDialog(bool private) {
    showDialog(
      context: context,
      builder: (context) => Dialog(
            child: Material(
              borderRadius: BorderRadius.circular(8.0),
              clipBehavior: Clip.antiAlias,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Text(
                      'Set a new tax rule',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: 20.0,
                      ),
                    ),
                    SizedBox(height: 12.0),
                    Text(
                        'Current visibility: ${private ? 'Private' : 'Public'}'),
                    SizedBox(height: 24.0),
                    Material(
                      elevation: 6.0,
                      clipBehavior: Clip.antiAlias,
                      borderRadius: BorderRadius.circular(4.0),
                      color: Theme.of(context).accentColor,
                      child: InkWell(
                        onTap: () async {
                          Navigator.pop(context);
                          _showLoadingDialog();
                          AuthManager.instance.analytics
                              .logEvent(name: 'set_visibility');

                          String state = private ? 'public' : 'private';
                          await RedditManager.instance
                              .execCommand('!set$state');

                          await Future.delayed(Duration(seconds: 10));

                          Navigator.pop(context);
                          setState(() => firmMem = AsyncMemoizer());
                        },
                        child: Padding(
                          padding: EdgeInsets.all(12.0),
                          child: Text(
                            'Set ${private ? 'Public' : 'Private'}',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 8.0),
                    Material(
                      clipBehavior: Clip.antiAlias,
                      borderRadius: BorderRadius.circular(4.0),
                      color: Colors.black12,
                      child: InkWell(
                        onTap: () => Navigator.pop(context),
                        child: Padding(
                          padding: EdgeInsets.all(12.0),
                          child: Text(
                            'Cancel',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.black,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            backgroundColor: Colors.transparent,
          ),
    );
  }

  _showUpgradeDialog(int currentRank) {
    int baseSize = 8;
    int baseCost = 2000000;

    currentRank = currentRank + 1;

    showDialog(
      context: context,
      builder: (context) => Dialog(
            child: Material(
              borderRadius: BorderRadius.circular(8.0),
              clipBehavior: Clip.antiAlias,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Text(
                      'Upgrade the firm',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: 20.0,
                      ),
                    ),
                    SizedBox(height: 12.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Flexible(
                          flex: 1,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text(
                                'Rank: $currentRank',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16.0,
                                ),
                              ),
                              Text('Size: ${8 * pow(2, currentRank - 1)}'),
                              Text(
                                  'Associates: ${8 * pow(2, currentRank - 1) ~/ 2 - 2}'),
                              Text('Executives: ${pow(2, currentRank)}'),
                            ],
                          ),
                        ),
                        Icon(
                          Icons.keyboard_arrow_right,
                          color: Theme.of(context).accentColor,
                        ),
                        Flexible(
                          flex: 1,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text(
                                'Rank: ${currentRank + 1}',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16.0,
                                ),
                              ),
                              Text('Size: ${8 * pow(2, currentRank)}'),
                              Text(
                                  'Associates: ${8 * pow(2, currentRank) ~/ 2 - 2}'),
                              Text('Executives: ${pow(2, currentRank + 1)}'),
                            ],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 8.0),
                    SizedBox(height: 24.0),
                    Material(
                      elevation: 6.0,
                      clipBehavior: Clip.antiAlias,
                      borderRadius: BorderRadius.circular(4.0),
                      color: Theme.of(context).accentColor,
                      child: InkWell(
                        onTap: () async {
                          Navigator.pop(context);
                          _showLoadingDialog();
                          AuthManager.instance.analytics
                              .logEvent(name: 'upgrade_firm');

                          await RedditManager.instance.execCommand('!upgrade');
                          await Future.delayed(Duration(seconds: 10));

                          Navigator.pop(context);
                          setState(() => firmMem = AsyncMemoizer());
                        },
                        child: Padding(
                          padding: EdgeInsets.all(12.0),
                          child: Text(
                            'Upgrade (F¢ ${Misc.addCommaToNumber((baseCost * pow(currentRank, 2)).toStringAsFixed(2))})',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 8.0),
                    Material(
                      clipBehavior: Clip.antiAlias,
                      borderRadius: BorderRadius.circular(4.0),
                      color: Colors.black12,
                      child: InkWell(
                        onTap: () => Navigator.pop(context),
                        child: Padding(
                          padding: EdgeInsets.all(12.0),
                          child: Text(
                            'Cancel',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.black,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            backgroundColor: Colors.transparent,
          ),
    );
  }

  _showLoadingDialog() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => AlertDialog(
            content: Row(
              children: <Widget>[
                CircularProgressIndicator(),
                SizedBox(width: 12.0),
                Expanded(child: Text('Please wait...')),
              ],
            ),
          ),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (AuthManager.instance.localUser == null) {
      return FutureBuilder(
        future: userMem.runOnce(() => AuthManager.instance.getUser()),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done &&
              snapshot.hasData) {
            final User user = snapshot.data;
            if (user.firm != null) {
              return _buildFirmDashboard(user);
            } else {
              return _buildEmptyState();
            }
          } else
            return Container();
        },
      );
    } else {
      final User user = AuthManager.instance.localUser;
      if (user.firm != null) {
        return _buildFirmDashboard(user);
      } else {
        return _buildEmptyState();
      }
    }
  }

  Widget _buildFirmDashboard(User user) {
    usefulnessMem.runOnce(() => computeUsefulness(user.firm));

    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        CustomScrollView(
          slivers: <Widget>[
            SliverToBoxAdapter(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: AspectRatio(
                  aspectRatio: 16 / 9,
                  child: Material(
                    color: Theme.of(context).accentColor,
                    elevation: 6.0,
                    shadowColor: Colors.black38,
                    borderRadius: BorderRadius.circular(8.0),
                    clipBehavior: Clip.antiAlias,
                    child: FutureBuilder(
                      future:
                          firmMem.runOnce(() => ApiManager.getFirm(user.firm)),
                      builder: (context, snapshot) {
                        if (snapshot.connectionState == ConnectionState.done &&
                            snapshot.hasData) {
                          final firm = snapshot.data as Firm;
                          return InkWell(
                            onTap: () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (_) =>
                                        FirmsListPage(firmId: firm.id),
                                  ),
                                ).then(
                                  (_) {
                                    AuthManager.instance.analytics
                                        .setCurrentScreen(
                                      screenName: '${FirmsPage.routeName}',
                                    );
                                  },
                                ),
                            child: Stack(
                              fit: StackFit.expand,
                              children: <Widget>[
                                Image.asset(
                                  'res/img/bank.jpg',
                                  fit: BoxFit.cover,
                                ),
                                Container(color: Colors.black54),
                                Padding(
                                  padding: const EdgeInsets.all(32.0),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Icon(
                                            Icons.equalizer,
                                            color: Colors.white,
                                            size: 16.0,
                                          ),
                                          SizedBox(width: 4.0),
                                          FutureBuilder(
                                            future: firmRankMem.runOnce(() =>
                                                ApiManager.getFirmRank(
                                                    firm.id)),
                                            builder: (context, snapshot) {
                                              if (snapshot.connectionState ==
                                                      ConnectionState.done &&
                                                  snapshot.hasData) {
                                                return Text(
                                                  '${Misc.addCardinal(snapshot.data)}',
                                                  textAlign: TextAlign.center,
                                                  maxLines: 1,
                                                  style: TextStyle(
                                                    // fontSize: 12.0,
                                                    color: Colors.white,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                );
                                              } else
                                                return Text('...');
                                            },
                                          ),
                                        ],
                                      ),
                                      SizedBox(height: 16.0),
                                      AutoSizeText(
                                        firm.name,
                                        textAlign: TextAlign.center,
                                        maxLines: 2,
                                        style: TextStyle(
                                          fontSize: 32.0,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      AutoSizeText(
                                        'F¢ ${Misc.addCommaToNumber(firm.balance.toStringAsFixed(2))}',
                                        textAlign: TextAlign.center,
                                        maxLines: 1,
                                        style: TextStyle(
                                          fontSize: 16.0,
                                          color: Colors.white70,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          );
                        } else
                          return Center(
                            child: CircularProgressIndicator(),
                          );
                      },
                    ),
                  ),
                ),
              ),
            ),
            SliverToBoxAdapter(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Material(
                  color: Colors.white,
                  elevation: 6.0,
                  shadowColor: Colors.black38,
                  borderRadius: BorderRadius.circular(8.0),
                  clipBehavior: Clip.antiAlias,
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Text(
                          'Firm revenue this week',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(
                          'From ${getLastFriday().month.toString().padLeft(2, '0')}/${getLastFriday().day.toString().padLeft(2, '0')} to ${getLastFriday().add(Duration(days: 7)).month.toString().padLeft(2, '0')}/${getLastFriday().add(Duration(days: 7)).day.toString().padLeft(2, '0')}',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.black54,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(height: 16.0),
                        SizedBox.fromSize(
                          size: Size.fromRadius(128.0),
                          child: Stack(
                            children: <Widget>[
                              Align(
                                alignment: Alignment.center,
                                child: SizedBox.fromSize(
                                  size: Size.fromRadius(126.0),
                                  child: Material(
                                    shape: CircleBorder(),
                                    color: Colors.white,
                                    elevation: 6.0,
                                  ),
                                ),
                              ),
                              Center(
                                child: usersPerc != null
                                    ? charts.PieChart(
                                        [
                                          charts.Series(
                                            id: 'Users',
                                            domainFn: (val, _) =>
                                                val.keys.first.username,
                                            measureFn: (val, _) =>
                                                val.values.first.isNaN
                                                    ? 1
                                                    : val.values.first,
                                            data: usersPerc,
                                            labelAccessorFn: (val, _) {
                                              double money = val.values.first;

                                              if (money.isNaN) {
                                                print('nan');
                                                return '${0}%\n${val.keys.first.username}';
                                              } else if (money.isInfinite) {
                                                print('infinite');
                                                return '...%\n${val.keys.first.username}';
                                              } else
                                                return '${money.round()}%\n${val.keys.first.username}';
                                            },
                                            insideLabelStyleAccessorFn:
                                                (val, _) =>
                                                    charts.TextStyleSpec(
                                                      color: charts.Color.white,
                                                      fontSize: 10,
                                                      fontFamily: 'GoogleSans',
                                                    ),
                                            colorFn: (_, i) => charts.Color(
                                                  r: (Theme.of(context)
                                                              .accentColor
                                                              .red *
                                                          1.1)
                                                      .round(),
                                                  g: (Theme.of(context)
                                                              .accentColor
                                                              .green *
                                                          1.1)
                                                      .round(),
                                                  b: (Theme.of(context)
                                                              .accentColor
                                                              .blue *
                                                          1.1)
                                                      .round(),
                                                  a: min(
                                                      (((usersPerc.length - i) /
                                                                      usersPerc
                                                                          .length +
                                                                  0.3) *
                                                              255)
                                                          .round(),
                                                      255),
                                                ),
                                          ),
                                        ],
                                        defaultRenderer:
                                            charts.ArcRendererConfig(
                                          arcRendererDecorators: [
                                            charts.ArcLabelDecorator(),
                                          ],
                                          arcWidth: 96,
                                        ),
                                        layoutConfig: charts.LayoutConfig(
                                          bottomMarginSpec:
                                              charts.MarginSpec.fixedPixel(0),
                                          leftMarginSpec:
                                              charts.MarginSpec.fixedPixel(0),
                                          rightMarginSpec:
                                              charts.MarginSpec.fixedPixel(0),
                                          topMarginSpec:
                                              charts.MarginSpec.fixedPixel(0),
                                        ),
                                      )
                                    : CircularProgressIndicator(),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            SliverToBoxAdapter(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Material(
                  color: Theme.of(context).accentColor,
                  elevation: 6.0,
                  shadowColor: Colors.black38,
                  borderRadius: BorderRadius.circular(8.0),
                  clipBehavior: Clip.antiAlias,
                  child: ListTile(
                    leading: Icon(Icons.account_circle, color: Colors.white),
                    title: Text(
                      user.username,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20.0,
                        // fontWeight: FontWeight.bold,
                      ),
                    ),
                    subtitle: Text(
                      user.firmRole
                          .toString()
                          .split('.')[1]
                          .replaceAll('_', ' '),
                      style: TextStyle(
                        fontSize: 10.0,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    trailing: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Text(
                          Misc.addCardinal(getPosInFirm(user.username)),
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 16.0,
                          ),
                        ),
                        Text(
                          'Rank in firm',
                          style: TextStyle(
                            fontSize: 12.0,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            FutureBuilder(
                future: firmMem.runOnce(() => ApiManager.getFirm(user.firm)),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done &&
                      snapshot.hasData) {
                    final firm = snapshot.data as Firm;
                    return SliverGrid.count(
                      crossAxisCount: 2,
                      childAspectRatio: 1.1,
                      children: <Widget>[
                        _buildGridTile(
                          Icons.attach_money,
                          'Next payout',
                          'M¢ ${Misc.abbrNum(calculateNextPayout(firm, user))}',
                        ),
                        _buildGridTile(
                          Icons.people,
                          'Members',
                          '${firm.size} members',
                          onTap: usefulness != null && usefulness.length > 0
                              ? () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (_) =>
                                          MembersPage(users: usefulness),
                                    ),
                                  ).then((_) {
                                    AuthManager.instance.analytics
                                        .setCurrentScreen(
                                      screenName: '${FirmsPage.routeName}',
                                    );
                                  });
                                }
                              : null,
                        ),
                        _buildGridTile(
                          Icons.trending_up,
                          'Tax',
                          '${firm.tax}%',
                          permissions: [FirmRole.CFO, FirmRole.CEO],
                          userRole: user.firmRole,
                          onTap: () => _showTaxDialog(firm.tax),
                        ),
                        _buildGridTile(
                          Icons.remove_red_eye,
                          'Visibility',
                          firm.private ? 'Private' : 'Public',
                          permissions: [FirmRole.COO, FirmRole.CEO],
                          userRole: user.firmRole,
                          onTap: () => _showVisibilityDialog(firm.private),
                        ),
                        _buildGridTile(
                          Icons.layers,
                          'Level',
                          '${firm.rank + 1}',
                          permissions: [FirmRole.CFO, FirmRole.CEO],
                          userRole: user.firmRole,
                          onTap: () => _showUpgradeDialog(firm.rank),
                        ),
                      ],
                    );
                  } else
                    return SliverToBoxAdapter(
                      child: Center(child: CircularProgressIndicator()),
                    );
                }),
            SliverToBoxAdapter(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Material(
                  elevation: 6.0,
                  clipBehavior: Clip.antiAlias,
                  borderRadius: BorderRadius.circular(8.0),
                  color: Colors.red,
                  child: InkWell(
                    onTap: () async {
                      _showLoadingDialog();
                      AuthManager.instance.analytics
                          .logEvent(name: 'quit_firm');

                      UserManager.instance.unsubscribeFirmChat();

                      await RedditManager.instance.execCommand('!leavefirm');
                      await Future.delayed(Duration(seconds: 10));

                      Navigator.pop(context);
                      setState(
                          () => AuthManager.instance.localUser.firm = null);
                    },
                    child: Padding(
                      padding: EdgeInsets.all(14.0),
                      child: Text(
                        'Quit firm',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            SliverToBoxAdapter(child: SizedBox(height: 64.0)),
          ],
        ),
        Align(
          alignment: Alignment.bottomRight,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: FloatingActionButton.extended(
              onPressed: () async {
                final Firm firm = await firmMem.future;

                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (_) => MessagesPage(
                          name: user.username,
                          firm: firm.name,
                          firmId: user.firm,
                        ),
                  ),
                );
              },
              backgroundColor: Colors.black,
              label: Text('Chat'),
              icon: Icon(Icons.message),
            ),
          ),
        ),
      ],
    );
  }

  Padding _buildEmptyState() {
    final bool canCreateFirm =
        AuthManager.instance.localUser.balance >= 1000000;

    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          FlatButton(
            onPressed: () {
              launch(
                r'https://www.reddit.com/r/MemeEconomy/comments/aibosf/introducing_firms_to_the_meme_economy/',
              );
            },
            child: Text('Don\'t know what this is about? Learn more here'),
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                AspectRatio(
                  aspectRatio: 16 / 9,
                  child: Image.asset('res/img/firms.png'),
                ),
                Column(
                  children: <Widget>[
                    Text(
                      'Join a firm and get serious',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 24.0,
                      ),
                    ),
                    SizedBox(height: 16.0),
                    Text(
                      'It\'s easier to survive and thrive in the meme economy market within a firm.',
                      textAlign: TextAlign.center,
                      style: TextStyle(),
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Material(
                      elevation: 6.0,
                      clipBehavior: Clip.antiAlias,
                      borderRadius: BorderRadius.circular(8.0),
                      color: Theme.of(context).accentColor,
                      child: InkWell(
                        onTap: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (_) => FirmsListPage(),
                              ),
                            ).then(
                              (_) {
                                AuthManager.instance.analytics.setCurrentScreen(
                                  screenName: '${FirmsPage.routeName}',
                                );
                              },
                            ),
                        child: Padding(
                          padding: EdgeInsets.all(14.0),
                          child: Text(
                            'Join a firm',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Material(
                      elevation: canCreateFirm ? 6.0 : 0.0,
                      clipBehavior: Clip.antiAlias,
                      borderRadius: BorderRadius.circular(8.0),
                      color: canCreateFirm
                          ? Theme.of(context).accentColor
                          : Colors.black12,
                      child: InkWell(
                        onTap: canCreateFirm ? _showCreateFirmDialog : null,
                        child: Padding(
                          padding: EdgeInsets.all(14.0),
                          child: Text(
                            'Create a firm (M¢ 1,000,000.00)',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color:
                                  canCreateFirm ? Colors.white : Colors.black,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class TaxDialog extends StatefulWidget {
  final int currentVal;
  final Function(int) onNewTax;

  const TaxDialog({Key key, this.currentVal, this.onNewTax}) : super(key: key);

  @override
  _TaxDialogState createState() => _TaxDialogState();
}

class _TaxDialogState extends State<TaxDialog> {
  int value;
  @override
  void initState() {
    super.initState();
    value = widget.currentVal;
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.circular(8.0),
      clipBehavior: Clip.antiAlias,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text(
              'Set a new tax rule',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.black,
                fontSize: 20.0,
              ),
            ),
            SizedBox(height: 12.0),
            Text('Current tax: ${widget.currentVal}%'),
            Text('New tax: $value%'),
            SizedBox(height: 12.0),
            Slider(
              activeColor: Theme.of(context).accentColor,
              onChanged: (newVal) => setState(() => value = newVal.round()),
              value: value.toDouble(),
              divisions: 70,
              min: 5,
              max: 75,
            ),
            SizedBox(height: 24.0),
            Material(
              elevation: 6.0,
              clipBehavior: Clip.antiAlias,
              borderRadius: BorderRadius.circular(4.0),
              color: Theme.of(context).accentColor,
              child: InkWell(
                onTap: () {
                  Navigator.pop(context);
                  widget.onNewTax(value);
                },
                child: Padding(
                  padding: EdgeInsets.all(12.0),
                  child: Text(
                    'Set new tax',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(height: 8.0),
            Material(
              clipBehavior: Clip.antiAlias,
              borderRadius: BorderRadius.circular(4.0),
              color: Colors.black12,
              child: InkWell(
                onTap: () => Navigator.pop(context),
                child: Padding(
                  padding: EdgeInsets.all(12.0),
                  child: Text(
                    'Cancel',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.black,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class CreateFirmDialog extends StatefulWidget {
  final Function(String) onCreatePress;
  const CreateFirmDialog({Key key, this.onCreatePress}) : super(key: key);

  @override
  _CreateFirmDialogState createState() => _CreateFirmDialogState();
}

class _CreateFirmDialogState extends State<CreateFirmDialog> {
  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.circular(8.0),
      clipBehavior: Clip.antiAlias,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text(
              'Create a firm',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.black,
                fontSize: 20.0,
              ),
            ),
            SizedBox(height: 12.0),
            TextField(
              controller: controller,
              decoration: InputDecoration(
                hintText: 'Firm\'s name',
              ),
            ),
            SizedBox(height: 24.0),
            Material(
              elevation: 6.0,
              clipBehavior: Clip.antiAlias,
              borderRadius: BorderRadius.circular(4.0),
              color: Theme.of(context).accentColor,
              child: InkWell(
                onTap: () {
                  Navigator.pop(context);
                  widget.onCreatePress(controller.text.trim());
                },
                child: Padding(
                  padding: EdgeInsets.all(12.0),
                  child: Text(
                    'Create firm (M¢ 1,000,000.00)',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(height: 8.0),
            Material(
              clipBehavior: Clip.antiAlias,
              borderRadius: BorderRadius.circular(4.0),
              color: Colors.black12,
              child: InkWell(
                onTap: () => Navigator.pop(context),
                child: Padding(
                  padding: EdgeInsets.all(12.0),
                  child: Text(
                    'Cancel',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.black,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
