import 'package:dankbank/managers/auth_manager.dart';
import 'package:dankbank/managers/reddit_manager.dart';
import 'package:dankbank/misc.dart';
import 'package:dankbank/models/user.dart';
import 'package:dankbank/pages/stocks_page/stocks_page.dart';
import 'package:flutter/material.dart';

class MembersPage extends StatefulWidget {
  final Map<User, double> users;
  const MembersPage({Key key, this.users}) : super(key: key);

  @override
  _MembersPageState createState() => _MembersPageState();

  static const String routeName = '/members_page';
}

class _MembersPageState extends State<MembersPage> {
  List<Map<User, double>> users = [];

  @override
  void initState() {
    super.initState();
    AuthManager.instance.analytics
        .setCurrentScreen(screenName: '${MembersPage.routeName}');

    widget.users.forEach((key, val) => users.add({key: val}));
    users.sort((a, b) => b.values.single.compareTo(a.values.single));
  }

  _showPromoteDialog(String name) async {
    showDialog(
      context: context,
      builder: (context) => Dialog(
            child: Material(
              borderRadius: BorderRadius.circular(8.0),
              clipBehavior: Clip.antiAlias,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Text(
                      'Do you want to promote $name?',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: 20.0,
                      ),
                    ),
                    SizedBox(height: 24.0),
                    Material(
                      elevation: 6.0,
                      clipBehavior: Clip.antiAlias,
                      borderRadius: BorderRadius.circular(4.0),
                      color: Theme.of(context).accentColor,
                      child: InkWell(
                        onTap: () async {
                          Navigator.pop(context);
                          _showLoadingDialog();

                          await RedditManager.instance
                              .execCommand('!promote $name');
                          Navigator.pop(context);

                          setState(() {
                            final user = users
                                .firstWhere(
                                  (map) => map.keys.single.username == name,
                                )
                                .keys
                                .single;
                            if (user != null) {
                              user.firmRole =
                                  FirmRole.values[user.firmRole.index + 1];
                            }
                          });
                        },
                        child: Padding(
                          padding: EdgeInsets.all(12.0),
                          child: Text(
                            'Promote $name',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 8.0),
                    Material(
                      clipBehavior: Clip.antiAlias,
                      borderRadius: BorderRadius.circular(4.0),
                      color: Colors.black12,
                      child: InkWell(
                        onTap: () => Navigator.pop(context),
                        child: Padding(
                          padding: EdgeInsets.all(12.0),
                          child: Text(
                            'Cancel',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.black,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            backgroundColor: Colors.transparent,
          ),
    );
  }

  _showDemoteDialog(String name) async {
    showDialog(
      context: context,
      builder: (context) => Dialog(
            child: Material(
              borderRadius: BorderRadius.circular(8.0),
              clipBehavior: Clip.antiAlias,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Text(
                      'Do you want to demote $name?',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: 20.0,
                      ),
                    ),
                    SizedBox(height: 24.0),
                    Material(
                      elevation: 6.0,
                      clipBehavior: Clip.antiAlias,
                      borderRadius: BorderRadius.circular(4.0),
                      color: Theme.of(context).accentColor,
                      child: InkWell(
                        onTap: () async {
                          Navigator.pop(context);
                          _showLoadingDialog();

                          await RedditManager.instance
                              .execCommand('!demote $name');

                          Navigator.pop(context);

                          setState(() {
                            final user = users
                                .firstWhere(
                                  (map) => map.keys.single.username == name,
                                )
                                .keys
                                .single;
                            if (user != null) {
                              user.firmRole =
                                  FirmRole.values[user.firmRole.index - 1];
                            }
                          });
                        },
                        child: Padding(
                          padding: EdgeInsets.all(12.0),
                          child: Text(
                            'Demote $name',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 8.0),
                    Material(
                      clipBehavior: Clip.antiAlias,
                      borderRadius: BorderRadius.circular(4.0),
                      color: Colors.black12,
                      child: InkWell(
                        onTap: () => Navigator.pop(context),
                        child: Padding(
                          padding: EdgeInsets.all(12.0),
                          child: Text(
                            'Cancel',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.black,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            backgroundColor: Colors.transparent,
          ),
    );
  }

  _showFireDialog(String name) async {
    showDialog(
      context: context,
      builder: (context) => Dialog(
            child: Material(
              borderRadius: BorderRadius.circular(8.0),
              clipBehavior: Clip.antiAlias,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Text(
                      'Do you want to fire $name?',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: 20.0,
                      ),
                    ),
                    SizedBox(height: 24.0),
                    Material(
                      elevation: 6.0,
                      clipBehavior: Clip.antiAlias,
                      borderRadius: BorderRadius.circular(4.0),
                      color: Theme.of(context).accentColor,
                      child: InkWell(
                        onTap: () async {
                          Navigator.pop(context);
                          _showLoadingDialog();

                          await RedditManager.instance
                              .execCommand('!fire $name');
                          Navigator.pop(context);

                          setState(() => users.removeWhere(
                              (map) => map.keys.single.username == name));
                        },
                        child: Padding(
                          padding: EdgeInsets.all(12.0),
                          child: Text(
                            'Fire $name',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 8.0),
                    Material(
                      clipBehavior: Clip.antiAlias,
                      borderRadius: BorderRadius.circular(4.0),
                      color: Colors.black12,
                      child: InkWell(
                        onTap: () => Navigator.pop(context),
                        child: Padding(
                          padding: EdgeInsets.all(12.0),
                          child: Text(
                            'Cancel',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.black,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            backgroundColor: Colors.transparent,
          ),
    );
  }

  _showLoadingDialog() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => AlertDialog(
            content: Row(
              children: <Widget>[
                CircularProgressIndicator(),
                SizedBox(width: 12.0),
                Expanded(child: Text('Please wait...')),
              ],
            ),
          ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Text('Members'),
      ),
      body: ListView.builder(
        itemBuilder: (context, i) {
          final localUser = AuthManager.instance.localUser;
          final user = users[i].keys.single;
          final money = users[i].values.single;
          final isLocalUser = localUser.username == user.username;

          final hasPromotePerm = localUser.firmRole == FirmRole.CEO ||
              (localUser.firmRole == FirmRole.COO &&
                      User.roleCompare(
                        FirmRole.Executive,
                        user.firmRole,
                        equal: false,
                      ) ||
                  ((localUser.firmRole == FirmRole.CFO ||
                          localUser.firmRole == FirmRole.Executive) &&
                      User.roleCompare(
                        FirmRole.Associate,
                        user.firmRole,
                        equal: false,
                      )));

          final hasDemotePerm = user.firmRole != FirmRole.Floor_Trader &&
              (localUser.firmRole == FirmRole.CEO ||
                  (localUser.firmRole == FirmRole.COO &&
                          User.roleCompare(
                            FirmRole.Executive,
                            user.firmRole,
                          ) ||
                      ((localUser.firmRole == FirmRole.CFO ||
                              localUser.firmRole == FirmRole.Executive) &&
                          User.roleCompare(
                            FirmRole.Associate,
                            user.firmRole,
                          ))));

          final hasFirePerm = localUser.firmRole == FirmRole.CEO ||
              (localUser.firmRole == FirmRole.COO &&
                      User.roleCompare(
                        FirmRole.Executive,
                        user.firmRole,
                      ) ||
                  ((localUser.firmRole == FirmRole.CFO ||
                          localUser.firmRole == FirmRole.Executive) &&
                      User.roleCompare(
                        FirmRole.Associate,
                        user.firmRole,
                      )));

          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Material(
              color: isLocalUser ? Theme.of(context).accentColor : Colors.white,
              elevation: 6.0,
              shadowColor: Colors.black38,
              borderRadius: BorderRadius.circular(8.0),
              clipBehavior: Clip.antiAlias,
              child: InkWell(
                onTap: isLocalUser
                    ? null
                    : () => Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (_) => StocksPage(
                                  username: user.username,
                                ),
                          ),
                        ).then((_) {
                          AuthManager.instance.analytics.setCurrentScreen(
                              screenName: '${MembersPage.routeName}');
                        }),
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            user.firmRole
                                .toString()
                                .split('.')[1]
                                .replaceAll('_', ' '),
                            style: TextStyle(
                              color: isLocalUser
                                  ? Colors.white
                                  : Theme.of(context).accentColor,
                              fontSize: 10.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(height: 8.0),
                          Text(
                            user.username,
                            style: TextStyle(
                              color: isLocalUser ? Colors.white : Colors.black,
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            'M¢ ${Misc.addCommaToNumber(money.toStringAsFixed(2))}',
                            style: TextStyle(
                              color:
                                  isLocalUser ? Colors.white70 : Colors.black45,
                              fontWeight: FontWeight.bold,
                            ),
                          )
                        ],
                      ),
                      user.firmRole != FirmRole.CEO
                          ? Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Opacity(
                                  opacity: hasFirePerm ? 1.0 : 0.3,
                                  child: IconButton(
                                    onPressed: hasFirePerm
                                        ? () => _showFireDialog(user.username)
                                        : null,
                                    icon: Icon(
                                      Icons.whatshot,
                                      color: isLocalUser
                                          ? Colors.white
                                          : Colors.red,
                                    ),
                                  ),
                                ),
                                Opacity(
                                  opacity: hasDemotePerm ? 1.0 : 0.3,
                                  child: IconButton(
                                    onPressed: hasDemotePerm
                                        ? () => _showDemoteDialog(user.username)
                                        : null,
                                    icon: Icon(
                                      Icons.trending_down,
                                      color: isLocalUser
                                          ? Colors.white
                                          : Colors.black,
                                    ),
                                  ),
                                ),
                                Opacity(
                                  opacity: hasPromotePerm ? 1.0 : 0.3,
                                  child: IconButton(
                                    onPressed: hasPromotePerm
                                        ? () =>
                                            _showPromoteDialog(user.username)
                                        : null,
                                    icon: Icon(
                                      Icons.trending_up,
                                      color: isLocalUser
                                          ? Colors.white
                                          : Colors.green,
                                    ),
                                  ),
                                ),
                              ],
                            )
                          : Container(),
                    ],
                  ),
                ),
              ),
            ),
          );
        },
        itemCount: users.length,
      ),
    );
  }
}
