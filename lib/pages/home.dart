import 'package:dankbank/managers/auth_manager.dart';
import 'package:dankbank/pages/bottom_navbar.dart';
import 'package:dankbank/pages/feed/feed_page.dart';
import 'package:dankbank/pages/firms/firms_page.dart';
import 'package:dankbank/pages/friends/friends_page.dart';
import 'package:dankbank/pages/settings/settings_page.dart';
import 'package:dankbank/pages/stocks_page/stocks_page.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _pages = [
    StocksPage(),
    FeedPage(),
    FriendsPage(),
    FirmsPage(),
    SettingsPage(),
  ];

  int _page = 0;

  _changePage(int i) => setState(() => _page = i);

  _showNotifDialog() {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) => Dialog(
            child: Material(
              borderRadius: BorderRadius.circular(8.0),
              clipBehavior: Clip.antiAlias,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Text(
                      'Do you want to enable notifications?',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: 20.0,
                      ),
                    ),
                    SizedBox(height: 8.0),
                    Text(
                      'You will receive a notification when your investments matures.',
                    ),
                    SizedBox(height: 24.0),
                    Material(
                      elevation: 6.0,
                      clipBehavior: Clip.antiAlias,
                      borderRadius: BorderRadius.circular(4.0),
                      color: Theme.of(context).accentColor,
                      child: InkWell(
                        onTap: () {
                          AuthManager.instance.analytics.logEvent(
                            name: 'enable_notifications',
                            parameters: {'state': true},
                          );
                          AuthManager.instance.setNotifEnable(true);
                          Navigator.pop(context);
                        },
                        child: Padding(
                          padding: EdgeInsets.all(12.0),
                          child: Text(
                            'Sure',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 8.0),
                    Material(
                      clipBehavior: Clip.antiAlias,
                      borderRadius: BorderRadius.circular(4.0),
                      color: Colors.black12,
                      child: InkWell(
                        onTap: () {
                          AuthManager.instance.analytics.logEvent(
                            name: 'enable_notifications',
                            parameters: {'state': false},
                          );

                          AuthManager.instance.setNotifEnable(false);
                          Navigator.pop(context);
                        },
                        child: Padding(
                          padding: EdgeInsets.all(12.0),
                          child: Text(
                            'No, thanks',
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.black),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            backgroundColor: Colors.transparent,
          ),
    );
  }

  @override
  void initState() {
    super.initState();

    if (AuthManager.instance.isNotifEnabled() == null) {
      Future.delayed(Duration(seconds: 1), () => _showNotifDialog());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _page == 0
          ? null
          : AppBar(
              elevation: 0.0,
              leading: Container(
                margin: EdgeInsets.all(12.0),
                decoration: BoxDecoration(border: Border.all(width: 2.0)),
                child: Icon(Icons.account_balance, size: 18.0),
              ),
              title: Text(
                'DankBank',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 24.0,
                  // color: Colors.black26,
                ),
              ),
            ),
      body: _pages[_page],
      bottomNavigationBar: BottomNavbar(
        actualItem: _page,
        onActualItemChanged: _changePage,
      ),
    );
  }
}
