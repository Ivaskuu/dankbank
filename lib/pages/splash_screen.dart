import 'package:dankbank/managers/auth_manager.dart';
import 'package:dankbank/pages/home.dart';
import 'package:dankbank/pages/onboarding/onboarding_page.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();

  static const String routeName = '/splash_screen';
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    AuthManager.instance.initPrefs().then((_) {
      AuthManager.instance.analytics
          .setCurrentScreen(screenName: '${SplashScreen.routeName}');

      if (AuthManager.instance.prefs.getString('username') != null) {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (_) => HomePage()),
        );
      } else {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (_) => OnboardingPage()),
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) => Scaffold();
}
