import 'package:dankbank/managers/auth_manager.dart';
import 'package:dankbank/pages/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(16.0),
      children: <Widget>[
        Text(
          'Settings',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 32.0,
          ),
        ),
        SizedBox(height: 16.0),
        Divider(),
        ListTile(
          onTap: () => launch(
              r'https://2b77b620-1309-4ef1-bf94-40f95181ed1a.htmlpasta.com'),
          title: Text('Privacy policy'),
          trailing: Icon(Icons.keyboard_arrow_right),
        ),
        ListTile(
          onTap: () => launch(
              r'https://2b77b620-1309-4ef1-bf94-40f95181ed1a.htmlpasta.com'),
          title: Text('Terms and conditions'),
          trailing: Icon(Icons.keyboard_arrow_right),
        ),
        ListTile(
          onTap: () => launch(r'https://www.patreon.com/memeinvestor_bot'),
          title: Text(
              'Support MemeInvestor_bot on Patreon'),
          trailing: Icon(Icons.keyboard_arrow_right),
        ),
        Divider(),
        ListTile(
          onTap: () => launch(
              r'https://www.reddit.com/message/compose?to=Ivaskuu&subject=About DankBank'),
          leading: Icon(Icons.bug_report),
          title: Text('Bug? Suggestion? Contact the dev!'),
          subtitle: Text('This will open Reddit'),
          trailing: Icon(Icons.keyboard_arrow_right),
        ),
        Divider(),
        ListTile(
          onTap: () async {
            await AuthManager.instance.prefs.clear();
            AuthManager.instance.localUser = null;

            Navigator.pushReplacement(
                context, MaterialPageRoute(builder: (_) => SplashScreen()));
          },
          title: Text('Logout'),
          trailing: Icon(Icons.exit_to_app),
        ),
      ],
    );
  }
}
