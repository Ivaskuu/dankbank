import 'dart:math';

import 'package:async/async.dart';
import 'package:dankbank/managers/api_manager.dart';
import 'package:dankbank/managers/auth_manager.dart';
import 'package:dankbank/misc.dart';
import 'package:dankbank/models/investment.dart';
import 'package:dankbank/models/user.dart';
import 'package:dankbank/pages/splash_screen.dart';
import 'package:dankbank/pages/stocks_page/widgets/investment_tile.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sparkline/flutter_sparkline.dart';
import 'package:auto_size_text/auto_size_text.dart';

class StocksPage extends StatefulWidget {
  final String username;
  const StocksPage({Key key, this.username}) : super(key: key);

  @override
  _StocksPageState createState() => _StocksPageState();

  static const String routeName = '/stocks_page';
}

class _StocksPageState extends State<StocksPage> {
  AsyncMemoizer userMem = AsyncMemoizer();
  AsyncMemoizer invMem = AsyncMemoizer();

  String username;

  @override
  void initState() {
    super.initState();
    username = widget.username ?? AuthManager.instance.getUsername();

    AuthManager.instance.analytics
        .setCurrentScreen(screenName: '${StocksPage.routeName}');

    // print(
    //     'Calculator: ' + ApiManager.calculateProfit(10579, 27, 100).toString());
  }

  _fetchInvestments() async {
    setState(() {
      userMem = AsyncMemoizer();
      invMem = AsyncMemoizer();
    });
  }

  _showRemoveUserDialog() {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
            title: Text('Do you want to remove $username from friends?'),
            actions: <Widget>[
              FlatButton(
                onPressed: () => Navigator.pop(context),
                child: Text('CANCEL'),
              ),
              FlatButton(
                onPressed: () {
                  AuthManager.instance.removeFriend(username);
                  Navigator.pop(context);

                  Navigator.pop(context, true);
                },
                child: Text('REMOVE'),
              ),
            ],
          ),
    );
  }

  _showLoadingDialog() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => AlertDialog(
            title: Text('Opening your account...'),
            content: Row(
              children: <Widget>[
                CircularProgressIndicator(),
                SizedBox(width: 8.0),
                Expanded(child: Text('This could take up to 20 seconds...')),
              ],
            ),
          ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final body = RefreshIndicator(
      onRefresh: () => _fetchInvestments(),
      child: CustomScrollView(
        slivers: <Widget>[
          SliverToBoxAdapter(
            child: Container(
              color: Theme.of(context).accentColor,
              padding: EdgeInsets.all(16.0).copyWith(top: 24.0, bottom: 0.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  widget.username == null
                      ? Padding(
                          padding: EdgeInsets.only(top: 16.0, bottom: 8.0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Text(
                                'Welcome back, ',
                                style: TextStyle(
                                  fontSize: 19.0,
                                  color: Colors.white,
                                ),
                              ),
                              Text(
                                'u/$username',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 21.0,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                        )
                      : Container(),
                  SizedBox.fromSize(
                    size: Size.fromHeight(210.0),
                    child: Material(
                      elevation: 6.0,
                      shadowColor:
                          Theme.of(context).accentColor.withOpacity(0.3),
                      borderRadius: BorderRadius.circular(4.0),
                      clipBehavior: Clip.antiAlias,
                      color: Theme.of(context).accentColor,
                      child: FutureBuilder(
                        future:
                            userMem.runOnce(() => ApiManager.getUser(username)),
                        builder: (context, snapshot) {
                          if (snapshot.connectionState ==
                                  ConnectionState.done &&
                              snapshot.hasData) {
                            final user = snapshot.data as User;

                            return Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.all(16.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      AutoSizeText(
                                        'M¢ ' +
                                            Misc.addCommaToNumber(user.balance
                                                .toStringAsFixed(2)),
                                        maxLines: 1,
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white,
                                          fontSize: 42.0,
                                        ),
                                      ),
                                      FutureBuilder(
                                        future: invMem.runOnce(() =>
                                            ApiManager.getInvestments(
                                                username)),
                                        builder: (context, snapshot) {
                                          if (snapshot.connectionState ==
                                                  ConnectionState.done &&
                                              snapshot.hasData) {
                                            List<Investment> pendingInv =
                                                List.from(snapshot.data);

                                            pendingInv = pendingInv
                                                .where((inv) => !inv.done)
                                                .toList();

                                            final invested =
                                                pendingInv.length > 0
                                                    ? pendingInv
                                                        .map<double>(
                                                            (inv) => inv.amount)
                                                        .reduce((a, b) => a + b)
                                                    : 0;

                                            final balance =
                                                invested + user.balance;

                                            return Text(
                                              invested == 0
                                                  ? 'No pending investments'
                                                  : 'M¢ ' +
                                                      Misc.abbrNum(invested) +
                                                      ' (${((invested / balance) * 100).round()}%) invested',
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  color: Colors.white70),
                                            );
                                          } else
                                            return Container();
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                                FutureBuilder(
                                  future: invMem.runOnce(() =>
                                      ApiManager.getInvestments(username)),
                                  builder: (context, snapshot) {
                                    if (snapshot.connectionState ==
                                            ConnectionState.done &&
                                        snapshot.hasData) {
                                      List<Investment> investments =
                                          List.from(snapshot.data);

                                      investments
                                          .removeWhere((inv) => !inv.done);

                                      List<double> values = [];
                                      values = investments
                                          .map((inv) => inv.profit)
                                          .toList();

                                      values = values.reversed.toList();
                                      values = values
                                          .getRange(max(values.length - 20, 0),
                                              values.length)
                                          .toList();

                                      if (values.length > 1) {
                                        return Sparkline(
                                          data: values,
                                          lineColor: Colors.white70,
                                          fillMode: FillMode.below,
                                          fillGradient: LinearGradient(
                                            colors: [
                                              Colors.white70,
                                              Colors.white10,
                                              // Theme.of(context).accentColor
                                            ],
                                            begin: Alignment.topCenter,
                                            end: Alignment.bottomCenter,
                                          ),
                                        );
                                      } else {
                                        return Padding(
                                          padding: const EdgeInsets.only(
                                            bottom: 48.0,
                                            left: 24.0,
                                            right: 24.0,
                                          ),
                                          child: Text(
                                            'You need to have at least 2 done investments to see the chart',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: Colors.white70,
                                            ),
                                          ),
                                        );
                                      }
                                    }
                                    return Center(
                                        child: CircularProgressIndicator());
                                  },
                                ),
                              ],
                            );
                          } else
                            return Center(child: CircularProgressIndicator());
                        },
                      ),
                    ),
                  ),
                  SizedBox(height: widget.username == null ? 25.0 : 24.0),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'Transactions',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.white),
                    ),
                  ),
                  SizedBox(height: 8.0),
                ],
              ),
            ),
          ),
          FutureBuilder(
            future: invMem.runOnce(() => ApiManager.getInvestments(username)),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done &&
                  snapshot.hasData) {
                List<Investment> investments = List.from(snapshot.data);
                investments.sort((a, b) => b.time.compareTo(a.time));

                return SliverList(
                  delegate: SliverChildBuilderDelegate(
                    (context, i) {
                      return InvestmentTile(
                        investment: investments[i],
                        first: i == 0,
                        thisUser: widget.username == null,
                      );
                    },
                    childCount: investments.length,
                  ),
                );
              } else
                return SliverToBoxAdapter(
                  child: Center(child: CircularProgressIndicator()),
                );
            },
          ),
        ],
      ),
    );

    if (widget.username != null) {
      return Scaffold(
        appBar: AppBar(
          brightness: Brightness.dark,
          backgroundColor: Theme.of(context).accentColor,
          elevation: 0.0,
          leading: BackButton(color: Colors.white),
          title: Text(
            'u/${widget.username}',
            style: TextStyle(color: Colors.white),
          ),
          actions: <Widget>[
            IconButton(
              onPressed: _showRemoveUserDialog,
              color: Colors.white,
              icon: Icon(Icons.delete),
            ),
          ],
        ),
        body: body,
      );
    } else {
      if (AuthManager.instance.hasAccount) {
        return body;
      } else {
        return FutureBuilder(
          future: userMem.runOnce(() => ApiManager.getUser(username)),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done &&
                snapshot.hasData) {
              final user = snapshot.data as User;
              if (user.username != null && user.username.length > 0) {
                AuthManager.instance.hasAccount = true;
                return body;
              } else
                return Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      AppBar(
                        elevation: 0.0,
                        leading: Container(
                          margin: EdgeInsets.all(12.0),
                          decoration:
                              BoxDecoration(border: Border.all(width: 2.0)),
                          child: Icon(Icons.account_balance, size: 18.0),
                        ),
                        title: Text(
                          'DankBank',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 24.0,
                            // color: Colors.black26,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Image.asset('res/img/secretary.png'),
                            /* Text(
                            '👋',
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 32.0),
                          ), */
                            // SizedBox(height: 64.0),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                'Hey there, you\'re the new kid? Let me guess, you\'re here to make a fortune investing in memes.\n\nLet\'s open your account then!',
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 16.0),
                              ),
                            ),
                            // SizedBox(height: 64.0),
                            Material(
                              elevation: 6.0,
                              clipBehavior: Clip.antiAlias,
                              borderRadius: BorderRadius.circular(8.0),
                              color: Theme.of(context).accentColor,
                              child: InkWell(
                                onTap: () {
                                  _showLoadingDialog();
                                  ApiManager.initUser().then((_) {
                                    Navigator.pop(context);
                                    Navigator.pushReplacement(
                                        context,
                                        MaterialPageRoute(
                                            builder: (_) => SplashScreen()));
                                  });
                                },
                                child: Padding(
                                  padding: EdgeInsets.all(16.0),
                                  child: Text(
                                    'Open a new account',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                );
            } else
              return Center(
                child: CircularProgressIndicator(),
              );
          },
        );
      }
    }
  }
}
