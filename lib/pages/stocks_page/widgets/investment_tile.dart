import 'package:auto_size_text/auto_size_text.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:dankbank/managers/api_manager.dart';
import 'package:dankbank/managers/auth_manager.dart';
import 'package:dankbank/managers/formula.dart';
import 'package:dankbank/managers/notif_manager.dart';
import 'package:dankbank/pages/feed/post_page.dart';
import 'package:dankbank/pages/stocks_page/stocks_page.dart';
import 'package:draw/draw.dart';
import 'package:flutter/material.dart';
import 'package:async/async.dart';
import 'package:dankbank/managers/reddit_manager.dart';
import 'package:dankbank/misc.dart';
import 'package:dankbank/models/investment.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:url_launcher/url_launcher.dart';

class InvestmentTile extends StatefulWidget {
  final Investment investment;
  final bool first;
  final bool thisUser;

  const InvestmentTile({
    Key key,
    this.investment,
    this.first,
    this.thisUser,
  }) : super(key: key);

  @override
  State createState() => _InvestmentTileState();
}

class _InvestmentTileState extends State<InvestmentTile> {
  final postMem = AsyncMemoizer();
  final invMem = AsyncMemoizer();

  _usToHhMm(int us) {
    int milliseconds = (us ~/ 1000) % 1000;
    int seconds = (((us ~/ 1000) - milliseconds) ~/ 1000) % 60;
    int minutes =
        (((((us ~/ 1000) - milliseconds) ~/ 1000) - seconds) ~/ 60) % 60;
    int hours = ((((((us ~/ 1000) - milliseconds) ~/ 1000) - seconds) ~/ 60) -
            minutes) ~/
        60;

    return '${hours.toString().padLeft(2, '0')}:${minutes.toString().padLeft(2, '0')}';
  }

  @override
  void initState() {
    super.initState();
    if (widget.thisUser && !widget.investment.done) {
      NotifManager.instance.scheduleNotif(DateTime.fromMillisecondsSinceEpoch(
          (widget.investment.time * 1000) + 14400000));
    }
  }

  @override
  Widget build(BuildContext context) {
    final image = Material(
      color: Colors.white,
      borderRadius: BorderRadius.circular(8.0),
      elevation: 6.0,
      clipBehavior: Clip.antiAlias,
      shadowColor: Colors.black38,
      child: FutureBuilder(
        future: postMem.runOnce(() =>
            RedditManager.instance.getSubmission(widget.investment.postId)),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done &&
              snapshot.hasData) {
            final post = snapshot.data as Submission;

            return GestureDetector(
              onTap: () {
                AuthManager.instance.analytics.logEvent(name: 'preview_meme');

                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (_) => PostPage(post: post)),
                ).then((_) {
                  AuthManager.instance.analytics
                      .setCurrentScreen(screenName: '${StocksPage.routeName}');
                });
              },
              child: Image.network(post.url.toString(), fit: BoxFit.cover),
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );

    final doneInfos = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  Icons.arrow_upward,
                  size: 16.0,
                ),
                SizedBox(width: 8.0),
                Text(
                  '${widget.investment.finalUpvotes} (+${widget.investment.finalUpvotes - widget.investment.initUpvotes}) upvotes',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    // fontSize: 48.0,
                  ),
                ),
              ],
            ),
            SizedBox(height: 4.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  Icons.access_time,
                  color: Colors.black54,
                  size: 16.0,
                ),
                SizedBox(width: 8.0),
                Text(
                  timeago.format(
                    DateTime.fromMillisecondsSinceEpoch(
                      widget.investment.time * 1000,
                    ),
                  ),
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black54,
                  ),
                ),
              ],
            ),
          ],
        ),
        SizedBox(width: 8.0),
        Expanded(
          child: AutoSizeText(
            widget.investment.profit > 0
                ? '+' +
                    Misc.addCommaToNumber(widget.investment.profit.toString()) +
                    ' M¢'
                : Misc.addCommaToNumber(widget.investment.profit.toString()) +
                    ' M¢',
            maxLines: 1,
            textAlign: TextAlign.right,
            style: TextStyle(
              color: widget.investment.profit >= 0 ? Colors.green : Colors.red,
              fontWeight: FontWeight.bold,
              fontSize: 24.0,
            ),
          ),
        ),
      ],
    );

    final pendingInfos = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                // Icon(
                //   Icons.arrow_upward,
                //   size: 16.0,
                // ),
                SizedBox(width: 4.0),
                Text(
                  'Invested ${Misc.abbrNum(widget.investment.amount)} M¢ at ${widget.investment.initUpvotes} upvotes',
                  // style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ],
            ),
            SizedBox(height: 4.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Icon(
                  Icons.attach_money,
                  size: 16.0,
                ),
                SizedBox(width: 4.0),
                FutureBuilder(
                  future: postMem.runOnce(() => RedditManager.instance
                      .getSubmission(widget.investment.postId)),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.done &&
                        snapshot.hasData) {
                      final post = snapshot.data as Submission;
                      final inv = widget.investment;

                      final double profit = Formula.calculate(
                          inv.amount, inv.initUpvotes, post.upvotes);

                      // 2 459 849 174
                      // 67 321 557 095

                      final String sign = profit >= 0 ? '+' : '';
                      final int perc = ((profit / inv.amount) * 100).round();

                      return Text(
                        '$sign$perc% ($sign${Misc.abbrNum(profit, 2)} M¢)',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      );
                    } else
                      return Container();
                  },
                ),
              ],
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              _usToHhMm(
                14400000000 -
                    (DateTime.now().difference(
                      DateTime.fromMillisecondsSinceEpoch(
                        widget.investment.time * 1000,
                      ),
                    ))
                        .inMicroseconds,
              ),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.black54,
              ),
            ),
            SizedBox(width: 4.0),
            Icon(
              Icons.access_time,
              color: Colors.black54,
              size: 16.0,
            ),
          ],
        ),
      ],
    );

    final chart = SizedBox(
      height: 100.0,
      child: FutureBuilder(
          future: postMem.runOnce(() =>
              RedditManager.instance.getSubmission(widget.investment.postId)),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done &&
                snapshot.hasData) {
              final post = snapshot.data as Submission;

              return FutureBuilder(
                  future: invMem.runOnce(
                    () => ApiManager.getMemeInvestmentsV2(
                          post.id,
                          minTime: DateTime.fromMillisecondsSinceEpoch(
                            widget.investment.time * 1000,
                          ),
                        ),
                  ),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.done &&
                        snapshot.hasData) {
                      final blue = charts.Color(
                        r: Theme.of(context).accentColor.red,
                        g: Theme.of(context).accentColor.green,
                        b: Theme.of(context).accentColor.blue,
                      );

                      if ((snapshot.data as List).length >= 2) {
                        return charts.TimeSeriesChart(
                          [
                            charts.Series<Investment, DateTime>(
                              id: 'Profit',
                              colorFn: (_, __) => blue,
                              domainFn: (Investment inv, _) =>
                                  DateTime.fromMillisecondsSinceEpoch(
                                    inv.time * 1000,
                                  ),
                              measureFn: (Investment inv, _) =>
                                  (Formula.calculate(
                                        widget.investment.amount,
                                        widget.investment.initUpvotes,
                                        inv.initUpvotes,
                                      ) /
                                      widget.investment.amount) *
                                  100,
                              data: snapshot.data,
                            ),
                          ],
                          animate: true,
                        );

                        /* return Sparkline(
                    data: snapshot.data
                        .map<double>((map) =>
                            (map.values.elementAt(0) as int) * 1.0)
                        .toList(),
                    lineColor: Theme.of(context).accentColor,
                    fillMode: FillMode.below,
                    fillGradient: LinearGradient(
                      colors: [
                        Colors.white70,
                        Colors.white10,
                        // Theme.of(context).accentColor
                      ],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                    ),
                  ); */
                      } else
                        return Center(child: Text('Not enough data'));
                    } else
                      return Center(child: CircularProgressIndicator());
                  });
            } else
              return Center(child: CircularProgressIndicator());
          }),
    );

    return Stack(
      children: <Widget>[
        Align(
          alignment: Alignment.topCenter,
          child: widget.first
              ? Container(
                  padding: EdgeInsets.only(top: 36.0),
                  color: Theme.of(context).accentColor,
                )
              : Container(),
        ),
        Padding(
          padding: EdgeInsets.all(8.0),
          child: SizedBox(
            height: widget.investment.done ? 300.0 : 400.0,
            child: Material(
              color: Colors.white,
              borderRadius: BorderRadius.circular(8.0),
              elevation: 6.0,
              clipBehavior: Clip.antiAlias,
              shadowColor: Colors.black38,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Expanded(child: image),
                  Padding(
                    padding: EdgeInsets.all(12.0),
                    child: widget.investment.done ? doneInfos : pendingInfos,
                  ),
                  widget.investment.done ? Container() : chart,
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
