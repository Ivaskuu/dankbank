import 'dart:math';

import 'package:dankbank/misc.dart';
import 'package:dankbank/pages/stocks_page/widgets/vector2.dart';
import 'package:flutter/material.dart';

enum BubbleChartType {
  Total,
  Investments,
}

class BubbleChart extends StatefulWidget {
  final List<num> investments;
  final BubbleChartType chartType;
  final double money;

  const BubbleChart({
    Key key,
    @required this.investments,
    @required this.chartType,
    this.money,
  }) : super(key: key);

  @override
  _BubbleChartState createState() => _BubbleChartState();
}

class _BubbleChartState extends State<BubbleChart> {
  final rand = Random();

  List<double> _prepareInvestments() {
    List<double> percentages = [];
    double total = 0.0;

    widget.investments.sort((a, b) => b.compareTo(a));

    if (widget.chartType == BubbleChartType.Total)
      total = widget.money;
    else
      widget.investments.forEach((amount) => total += amount);

    widget.investments.forEach((amount) => percentages.add(amount / total));
    return percentages;
  }

  double _calcCircleSize(perc, maxSize) {
    final maxArea = (pi * pow((maxSize / 2), 2)) * perc;
    return sqrt(maxArea / pi) * 2;
  }

  _getNotTouchingPos(
      List prevsSize, List prevsPos, double size, double maxSize) {
    final radius = size / 2;
    List<Vector2> notTouchingPoints = [];

    for (var y = 0; y < maxSize - size; y++) {
      for (var x = 0; x < maxSize - size; x++) {
        double minDist;

        for (var i = 0; i < prevsSize.length; i++) {
          final prevRadius = prevsSize[i] / 2;
          final prevPos = Vector2(
              x: prevsPos[i].x + prevRadius, y: prevsPos[i].y + prevRadius);

          final dist = sqrt(pow(prevPos.x - (x + radius), 2) +
                  pow(prevPos.y - (y + radius), 2)) -
              (prevRadius + radius);

          if (i == 0 || dist < minDist) {
            minDist = dist;
          }
        }

        if (minDist > 16.0) {
          notTouchingPoints.add(Vector2(x: x * 1.0, y: y * 1.0));
          // print(minDist);
        }
      }
    }

    return notTouchingPoints;
  }

  _getPos(size, maxSize) {
    Vector2 pos = Vector2();

    do {
      pos.x = rand.nextDouble() * maxSize;
      pos.y = rand.nextDouble() * maxSize;
    } while (!_checkInBounds(size, pos.x, pos.y, maxSize));

    return pos;
  }

  _checkInBounds(size, x, y, maxSize) =>
      x + size > maxSize || size + y > maxSize ? false : true;

  _positionCircles(List<double> circlesSize, double maxSize, {int ith}) {
    ith = (ith ?? 0) + 1;

    // If doesn't seem to find a pos, reduce size by 10%
    if (ith > 100) {
      print('reducing size');
      ith = 0;

      for (var i = 0; i < circlesSize.length; i++) circlesSize[i] *= 0.9;
    }

    List<Vector2> pos = List(circlesSize.length);
    List<List<Vector2>> notTouchingPoints = List(circlesSize.length);

    pos[0] = (_getPos(circlesSize[0], maxSize));

    for (var i = 1; i < circlesSize.length; i++) {
      final prevsSize = List.generate(i, (ith) => circlesSize[ith]);
      final prevsPos = List.generate(i, (ith) => pos[ith]);

      notTouchingPoints[i] =
          _getNotTouchingPos(prevsSize, prevsPos, circlesSize[i], maxSize);
      if (notTouchingPoints[i].length > 0) {
        pos[i] =
            notTouchingPoints[i][rand.nextInt(notTouchingPoints[i].length)];
      } else {
        // print('trying again');
        return _positionCircles(circlesSize, maxSize, ith: ith);
      }
    }

    return pos;
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        final size = constraints.maxWidth;
        final minSize = 96.0;

        final percentages = _prepareInvestments();
        final sizes = List.generate(percentages.length,
            (i) => max(_calcCircleSize(percentages[i], size), minSize));

        List pos = _positionCircles(sizes, size);

        return AspectRatio(
          aspectRatio: 1.0,
          child: Stack(
            fit: StackFit.expand,
            children: List.generate(
              pos.length,
              (i) => Positioned(
                    left: pos[i].x,
                    top: pos[i].y,
                    width: sizes[i],
                    height: sizes[i],
                    child: Material(
                      clipBehavior: Clip.antiAlias,
                      shape: CircleBorder(),
                      color: Color(0xFF0045A5),
                      child: Center(
                        child: Stack(
                          fit: StackFit.expand,
                          children: <Widget>[
                            Opacity(
                              opacity: 0.2,
                              child: Image.asset('res/meme.jpg',
                                  fit: BoxFit.cover),
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  '${(percentages[i] * 100).toStringAsFixed(0)}%',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                    fontSize: 22.0,
                                  ),
                                ),
                                Text(
                                  '${Misc.abbrNum(widget.investments[i])}M¢',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white70,
                                    fontSize: 15.0,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
            ),
          ),
        );
      },
    );
  }
}
