class Vector2 {
  double x;
  double y;

  Vector2({this.x: 0, this.y: 0});

  @override
  String toString() => '[$x, $y]';
}
