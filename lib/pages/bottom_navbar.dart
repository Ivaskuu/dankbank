import 'dart:async';

import 'package:dankbank/managers/auth_manager.dart';
import 'package:dankbank/managers/messages_manager.dart';
import 'package:flutter/material.dart';

class BottomNavbar extends StatefulWidget {
  final int actualItem;
  final Function(int) onActualItemChanged;

  const BottomNavbar({
    Key key,
    @required this.actualItem,
    @required this.onActualItemChanged,
  }) : super(key: key);

  @override
  _BottomNavbarState createState() => _BottomNavbarState();
}

class _BottomNavbarState extends State<BottomNavbar> {
  bool newMessage = false;

  @override
  void initState() {
    super.initState();

    // TODO
    /* Timer t;
    t = Timer.periodic(Duration(milliseconds: 200), (_) async {
      if (AuthManager.instance.localUser != null) {
        t.cancel();

        bool newMsg = await MessagesManager.instance.checkNewMsgs();
        setState(() {
          newMessage = newMsg;
        });
      }
    }); */
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox.fromSize(
      size: Size.fromHeight(64.0),
      child: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            IconButton(
              onPressed: () => changeItem(0),
              icon: Icon(
                Icons.show_chart,
                color: widget.actualItem == 0
                    ? Theme.of(context).accentColor
                    : Colors.black26,
              ),
            ),
            IconButton(
              onPressed: () => changeItem(1),
              icon: Icon(
                Icons.photo_library,
                color: widget.actualItem == 1
                    ? Theme.of(context).accentColor
                    : Colors.black26,
              ),
            ),
            IconButton(
              onPressed: () => changeItem(2),
              icon: Icon(
                Icons.people,
                color: widget.actualItem == 2
                    ? Theme.of(context).accentColor
                    : Colors.black26,
              ),
            ),
            IconButton(
              onPressed: () => changeItem(3),
              icon: newMessage
                  ? Stack(
                      alignment: Alignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.business,
                          color: widget.actualItem == 3
                              ? Theme.of(context).accentColor
                              : Colors.black26,
                        ),
                        Align(
                          alignment: Alignment(0.8, -0.8),
                          child: CircleAvatar(
                            backgroundColor: Colors.red,
                            radius: 4.0,
                          ),
                        ),
                      ],
                    )
                  : Icon(
                      Icons.business,
                      color: widget.actualItem == 3
                          ? Theme.of(context).accentColor
                          : Colors.black26,
                    ),
            ),
            IconButton(
              onPressed: () => changeItem(4),
              icon: Icon(
                Icons.settings,
                color: widget.actualItem == 4
                    ? Theme.of(context).accentColor
                    : Colors.black26,
              ),
            ),
          ],
        ),
      ),
    );
  }

  void changeItem(int i) {
    if (i != widget.actualItem) widget.onActualItemChanged(i);
  }
}
