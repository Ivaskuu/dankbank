import 'dart:async';

import 'package:dankbank/managers/auth_manager.dart';
import 'package:dankbank/managers/reddit_manager.dart';
import 'package:dankbank/pages/feed/widgets/post_card.dart';
import 'package:draw/draw.dart';
import 'package:flutter/material.dart';

class FeedPage extends StatefulWidget {
  @override
  _FeedPageState createState() => _FeedPageState();

  static const String routeName = '/feed_page';
}

class _FeedPageState extends State<FeedPage> {
  List<UserContent> submissions = [];
  StreamSubscription stream;

  @override
  void initState() {
    super.initState();
    listenForSubmissions();
    AuthManager.instance.analytics
        .setCurrentScreen(screenName: '${FeedPage.routeName}');
  }

  @override
  void dispose() {
    stream.cancel();
    super.dispose();
  }

  listenForSubmissions() async {
    try {
      stream = RedditManager.instance
          .getFeed(sort: AuthManager.instance.getSortType())
          .listen((post) {
        if (submissions.length > 0 && submissions.length % 10 == 0)
          stream.pause();

        mounted ? setState(() => submissions.add(post)) : submissions.add(post);
      });
    } catch (ex) {
      Future.delayed(Duration(seconds: 1), () => listenForSubmissions());
    }
  }

  resumeStream() => stream.resume();

  _resetStream() {
    setState(() => submissions.removeWhere((_) => true));
    listenForSubmissions();
  }

  _refreshMemes() async {
    setState(() {
      submissions = [];
      stream.cancel();
    });

    listenForSubmissions();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white70,
      child: RefreshIndicator(
        onRefresh: () => _refreshMemes(),
        child: CustomScrollView(
          slivers: <Widget>[
            SliverToBoxAdapter(
              child: SwitchListTile(
                title: Text('Sorting by'),
                subtitle: Text(
                  AuthManager.instance.getSortType() == Sort.hot
                      ? 'HOT'
                      : 'NEW',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                value: AuthManager.instance.getSortType() == Sort.hot
                    ? true
                    : false,
                onChanged: (val) {
                  setState(() => AuthManager.instance
                      .setSortType(val ? Sort.hot : Sort.newest));

                  _resetStream();
                },
              ),
            ),
            submissions.length > 0
                ? SliverList(
                    delegate: SliverChildBuilderDelegate((context, i) {
                      final post = submissions[i];
                      return PostCard(
                        post: post,
                        last: i == submissions.length - 1,
                        resumeStream: resumeStream,
                      );
                    }, childCount: submissions.length),
                  )
                : SliverToBoxAdapter(
                    child: Center(child: CircularProgressIndicator())),
          ],
        ),
      ),
    );
  }
}
