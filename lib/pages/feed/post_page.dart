import 'package:async/async.dart';
import 'package:dankbank/managers/auth_manager.dart';
import 'package:dankbank/pages/feed/invest_page.dart';
import 'package:dankbank/pages/feed/post_details_page.dart';
import 'package:dankbank/pages/feed/post_image_page.dart';
import 'package:draw/draw.dart';
import 'package:flutter/material.dart';
import 'package:preload_page_view/preload_page_view.dart';

class PostPage extends StatefulWidget {
  final Submission post;
  const PostPage({Key key, this.post}) : super(key: key);

  @override
  _PostPageState createState() => _PostPageState();

  static const String routeName = '/post_page';
}

class _PostPageState extends State<PostPage> {
  final AsyncMemoizer invMem = AsyncMemoizer();
  final AsyncMemoizer userMem = AsyncMemoizer();
  final AsyncMemoizer memeStatsMem = AsyncMemoizer();

  final PreloadPageController _controller =
      PreloadPageController(initialPage: 1);

  bool scrollable = true;

  onSwipeVertical() => Navigator.pop(context);
  setScrollable(bool newState) => setState(() => scrollable = newState);

  @override
  void initState() {
    super.initState();
    AuthManager.instance.analytics
        .setCurrentScreen(screenName: '${PostPage.routeName}');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PreloadPageView(
        physics: scrollable
            ? AlwaysScrollableScrollPhysics()
            : NeverScrollableScrollPhysics(),
        controller: _controller,
        preloadPagesCount: 3,
        children: <Widget>[
          PostDetailsPage(post: widget.post),
          GestureDetector(
            // onVerticalDragUpdate: (_) => onSwipeVertical(),
            child: PostImagePage(
              post: widget.post,
              onTap: setScrollable,
            ),
          ),
          InvestPage(post: widget.post),
        ],
      ),
    );
  }
}
