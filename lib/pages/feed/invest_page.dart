import 'package:async/async.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:dankbank/managers/api_manager.dart';
import 'package:dankbank/managers/auth_manager.dart';
import 'package:dankbank/managers/formula.dart';
import 'package:dankbank/managers/reddit_manager.dart';
import 'package:dankbank/misc.dart';
import 'package:dankbank/pages/feed/feed_page.dart';
import 'package:draw/draw.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class InvestPage extends StatefulWidget {
  final Submission post;
  const InvestPage({Key key, @required this.post}) : super(key: key);

  @override
  _InvestPageState createState() => _InvestPageState();
}

class _InvestPageState extends State<InvestPage> {
  final AsyncMemoizer statBotMem = AsyncMemoizer();

  final imgHeight = 250.0;
  double investFactor = 1.0;
  bool addPromoToComment = true;

  double balance;

  @override
  void initState() {
    super.initState();
    _initBalance();
    _updatePost();
  }

  _updatePost() async {
    await widget.post.refresh();
    if (mounted) setState(() {});
  }

  _invest() async {
    _showLoadingDialog();

    String postId = widget.post.id;
    String commentId = await RedditManager.instance.findBotCommentId(postId);

    await RedditManager.instance.votePost(false, id: widget.post.id);
    print('downvoted post');

    Future.delayed(Duration(seconds: 15), () async {
      await RedditManager.instance.votePost(true, id: widget.post.id);
      print('upvoted post');
    });

    final comment = await RedditManager.instance
        .postReply('!invest ${(investFactor * 100).round()}%', commentId);

    if (addPromoToComment)
      Future.delayed(Duration(seconds: 20),
          () => RedditManager.instance.editCommentPromo(comment));

    if (addPromoToComment)
      AuthManager.instance.analytics.logEvent(name: 'invest_promo');
    else
      AuthManager.instance.analytics.logEvent(name: 'invest_no_promo');

    Navigator.pop(context);

    print('Investment went successfully');
    _showInvestSuccess();
  }

  bool _canInvest() => (balance ?? 0) * (investFactor ?? 0) >= 100;

  _initBalance() async {
    final String username = AuthManager.instance.getUsername();
    final user = await ApiManager.getUser(username);

    if (mounted) setState(() => balance = user.balance);
  }

  _showInvestSuccess() {
    showDialog(
      context: context,
      builder: (context) => Dialog(
            child: Material(
              borderRadius: BorderRadius.circular(8.0),
              clipBehavior: Clip.antiAlias,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Text(
                      'The investment was successful',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: 20.0,
                      ),
                    ),
                    SizedBox(height: 4.0),
                    Text(
                        'Give it some seconds for the bot to see the comment.'),
                    SizedBox(height: 24.0),
                    Material(
                      elevation: 6.0,
                      clipBehavior: Clip.antiAlias,
                      borderRadius: BorderRadius.circular(4.0),
                      color: Theme.of(context).accentColor,
                      child: InkWell(
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.pop(context);
                          Navigator.pop(context);
                        },
                        child: Padding(
                          padding: EdgeInsets.all(12.0),
                          child: Text(
                            'Nice',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            backgroundColor: Colors.transparent,
          ),
    );
  }

  _showInvestDialog() {
    showDialog(
      context: context,
      builder: (context) => Dialog(
            child: Material(
              borderRadius: BorderRadius.circular(8.0),
              clipBehavior: Clip.antiAlias,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Text(
                      'Are you sure?',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: 20.0,
                      ),
                    ),
                    SizedBox(height: 4.0),
                    Text('Please confirm your transaction.'),
                    SizedBox(height: 24.0),
                    Material(
                      elevation: 6.0,
                      clipBehavior: Clip.antiAlias,
                      borderRadius: BorderRadius.circular(4.0),
                      color: Theme.of(context).accentColor,
                      child: InkWell(
                        onTap: _invest,
                        child: Padding(
                          padding: EdgeInsets.all(12.0),
                          child: Text(
                            'Confirm',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 12.0),
                    Material(
                      clipBehavior: Clip.antiAlias,
                      borderRadius: BorderRadius.circular(4.0),
                      color: Color(0x11000000),
                      child: InkWell(
                        onTap: () => Navigator.pop(context),
                        child: Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Text(
                            'Cancel',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            backgroundColor: Colors.transparent,
          ),
    );
  }

  _showLoadingDialog() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => AlertDialog(
            content: Row(
              children: <Widget>[
                CircularProgressIndicator(),
                SizedBox(width: 8.0),
                Expanded(child: Text('Please wait...')),
              ],
            ),
          ),
    );
  }

  _buildGridTile(IconData icon, String title, String content) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Material(
        color: Colors.white,
        elevation: 6.0,
        borderRadius: BorderRadius.circular(8.0),
        clipBehavior: Clip.antiAlias,
        shadowColor: Colors.black38,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              CircleAvatar(
                backgroundColor: Color(0x11000000),
                child: Icon(icon, color: Colors.black),
              ),
              ListTile(
                contentPadding: EdgeInsets.all(0.0),
                title: Text(title),
                subtitle: Text(
                  content,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 22.0,
                    color: Colors.black,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                floating: true,
                snap: true,
                automaticallyImplyLeading: false,
                elevation: 0.0,
                title: Text(
                  'Invest',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 32.0,
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Material(
                    elevation: 6.0,
                    shadowColor: Theme.of(context).accentColor.withOpacity(0.3),
                    borderRadius: BorderRadius.circular(8.0),
                    clipBehavior: Clip.antiAlias,
                    color: Theme.of(context).accentColor,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ListTile(
                        contentPadding: EdgeInsets.symmetric(horizontal: 8.0),
                        leading: Icon(
                          Icons.monetization_on,
                          color: Colors.white,
                        ),
                        title: Text(
                          'Balance',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                        subtitle: AutoSizeText(
                          balance == null
                              ? '... M¢'
                              : '${Misc.addCommaToNumber(balance.toStringAsFixed(2))} M¢',
                          maxLines: 1,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 24.0,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: Padding(
                  padding: const EdgeInsets.all(8.0).copyWith(top: 16.0),
                  child: Text(
                    'Meme asset analysis',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 14.0,
                    ),
                  ),
                ),
              ),
              SliverGrid.count(
                crossAxisCount: 2,
                childAspectRatio: 1.1,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Material(
                      color: Colors.white,
                      elevation: 6.0,
                      borderRadius: BorderRadius.circular(8.0),
                      clipBehavior: Clip.antiAlias,
                      shadowColor: Colors.black38,
                      child: Image.network(
                        widget.post.url.toString(),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  _buildGridTile(
                    Icons.arrow_upward,
                    'Upvotes',
                    widget.post.upvotes.toString(),
                  ),
                  _buildGridTile(
                    Icons.trending_flat,
                    'Break even',
                    Formula.breakEven(1, widget.post.upvotes).toString(),
                  ),
                  _buildGridTile(
                    Icons.trending_up,
                    'Max profit',
                    Formula.sigmoid_max(widget.post.upvotes)
                            .toStringAsFixed(2) +
                        'x',
                  ),
                ],
              ),
              FutureBuilder<Object>(
                future: statBotMem.runOnce(() => RedditManager.instance
                    .getMemeStatBot(submission: widget.post)),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done &&
                      snapshot.hasData) {
                    final Map data = snapshot.data;

                    if (data['loss'] != null ||
                        data['breakEven'] != null ||
                        data['profit'] != null) {
                      return SliverToBoxAdapter(
                        child: Padding(
                          padding:
                              const EdgeInsets.all(8.0).copyWith(top: 24.0),
                          child: Text(
                            'Meme format statistics',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 14.0,
                            ),
                          ),
                        ),
                      );
                    } else
                      return SliverToBoxAdapter(child: Container());
                  } else
                    return SliverToBoxAdapter(child: Container());
                },
              ),
              FutureBuilder<Object>(
                future: statBotMem.runOnce(() => RedditManager.instance
                    .getMemeStatBot(submission: widget.post)),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done &&
                      snapshot.hasData) {
                    final Map data = snapshot.data;

                    final List<Widget> children = [
                      data['loss'] != null
                          ? _buildGridTile(
                              Icons.trending_down,
                              'Loss',
                              '${data['loss'].round()}%',
                            )
                          : null,
                      data['breakEven'] != null
                          ? _buildGridTile(
                              Icons.trending_flat,
                              'Break even',
                              '${data['breakEven'].round()}%',
                            )
                          : null,
                      data['profit'] != null
                          ? _buildGridTile(
                              Icons.trending_up,
                              'Profit',
                              '${data['profit'].round()}%',
                            )
                          : null,
                    ];

                    children.removeWhere((val) => val == null);

                    return SliverGrid.count(
                      crossAxisCount: 2,
                      childAspectRatio: 1.1,
                      children: children,
                    );
                  } else
                    return SliverToBoxAdapter(child: Container());
                },
              ),
              SliverToBoxAdapter(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Divider(),
                      SizedBox(height: 16.0),
                      Material(
                        borderRadius: BorderRadius.circular(8.0),
                        clipBehavior: Clip.antiAlias,
                        color: Color(0x11000000),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0)
                              .copyWith(top: 16.0, bottom: 4.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(left: 16.0),
                                child: Text(
                                  'Invest amount: ${(investFactor * 100).round()}%',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                              SizedBox(height: 4.0),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Slider(
                                  divisions: 100,
                                  label: '${(investFactor * 100).round()}%',
                                  activeColor: Theme.of(context).accentColor,
                                  value: investFactor,
                                  onChanged: (val) =>
                                      setState(() => investFactor = val),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 16.0),
                      Material(
                        elevation: _canInvest() ? 6.0 : 0.0,
                        clipBehavior: Clip.antiAlias,
                        borderRadius: BorderRadius.circular(8.0),
                        color: _canInvest()
                            ? Theme.of(context).accentColor
                            : Color(0xFFEAEAEA),
                        child: InkWell(
                          onTap: () {
                            if (_canInvest()) {
                              _showInvestDialog();
                            }
                          },
                          child: Padding(
                            padding: EdgeInsets.all(16.0),
                            child: AutoSizeText(
                              balance == null
                                  ? 'Invest'
                                  : 'Invest ${Misc.addCommaToNumber((balance * investFactor).toStringAsFixed(2))} M¢',
                              textAlign: TextAlign.center,
                              maxLines: 1,
                              style: TextStyle(
                                color:
                                    _canInvest() ? Colors.white : Colors.black,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 24.0),
                      CheckboxListTile(
                        onChanged: (newVal) => setState(
                            () => addPromoToComment = !addPromoToComment),
                        value: addPromoToComment,
                        activeColor: Theme.of(context).accentColor,
                        controlAffinity: ListTileControlAffinity.leading,
                        subtitle: Text(
                          'Allow DankBank to add \'Invested with DankBank\' in the !invest comment. This would help us grow and we would be very grateful <3',
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  @override
  void dispose() {
    AuthManager.instance.analytics
        .setCurrentScreen(screenName: '${FeedPage.routeName}');

    super.dispose();
  }
}
