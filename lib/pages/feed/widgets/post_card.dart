import 'package:dankbank/managers/auth_manager.dart';
import 'package:dankbank/pages/feed/feed_page.dart';
import 'package:dankbank/pages/feed/post_page.dart';
import 'package:draw/draw.dart';
import 'package:flutter/material.dart';
import 'package:timeago/timeago.dart' as timeago;

class PostCard extends StatefulWidget {
  final Submission post;
  final bool last;
  final Function() resumeStream;

  const PostCard({Key key, this.post, this.last, this.resumeStream})
      : super(key: key);

  @override
  _PostCardState createState() => _PostCardState();
}

class _PostCardState extends State<PostCard> {
  @override
  Widget build(BuildContext context) {
    if (widget.last) {
      print('last');
      widget.resumeStream();
    }

    final image = GestureDetector(
      onTap: () {
        AuthManager.instance.analytics.logEvent(name: 'preview_meme');

        Navigator.push(
          context,
          MaterialPageRoute(builder: (_) => PostPage(post: widget.post)),
        ).then(
          (_) {
            AuthManager.instance.analytics
                .setCurrentScreen(screenName: '${FeedPage.routeName}');
          },
        );
      },
      child: Material(
        elevation: 6.0,
        shadowColor: Colors.black38,
        color: Colors.white,
        borderRadius: BorderRadius.circular(8.0),
        clipBehavior: Clip.antiAlias,
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Hero(
              tag: widget.post.url.toString(),
              child: Image.network(
                widget.post.url.toString(),
                fit: BoxFit.cover,
              ),
            ),
            Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [Colors.transparent, Color(0x99000000)],
                  begin: Alignment(0.0, 0.5),
                  end: Alignment.bottomCenter,
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomRight,
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 16.0,
                  vertical: 8.0,
                ),
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.arrow_upward,
                      color: Colors.white,
                      size: 16.0,
                    ),
                    SizedBox(width: 4.0),
                    Text(
                      '${widget.post.upvotes - widget.post.downvotes}',
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(width: 8.0),
                    Icon(
                      Icons.comment,
                      color: Colors.white,
                      size: 16.0,
                    ),
                    SizedBox(width: 4.0),
                    Text(
                      '${widget.post.numComments}',
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(width: 8.0),
                    Icon(
                      Icons.access_time,
                      color: Colors.white,
                      size: 16.0,
                    ),
                    SizedBox(width: 4.0),
                    Text(
                      timeago.format(widget.post.createdUtc),
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );

    final tile = Padding(
      padding: const EdgeInsets.all(8.0),
      child: Material(
        elevation: 6.0,
        shadowColor: Colors.black54,
        color: Colors.white,
        borderRadius: BorderRadius.circular(8.0),
        clipBehavior: Clip.antiAlias,
        child: image,
      ),
    );

    return AspectRatio(
      aspectRatio: 1.0,
      child: widget.post.runtimeType == UserContent
          ? FutureBuilder(
              future: widget.post.fetch(),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done &&
                    snapshot.hasData) {
                  if (widget.post.isSelf) return Container();
                  return tile;
                } else
                  return Center(child: CircularProgressIndicator());
              },
            )
          : tile,
    );
  }
}
