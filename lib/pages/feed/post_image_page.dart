import 'package:auto_size_text/auto_size_text.dart';
import 'package:dankbank/pages/stocks_page/stocks_page.dart';
import 'package:draw/draw.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:url_launcher/url_launcher.dart';

class PostImagePage extends StatefulWidget {
  final Submission post;
  final Function(bool) onTap;

  const PostImagePage({
    Key key,
    @required this.post,
    @required this.onTap,
  }) : super(key: key);

  @override
  _PostImagePageState createState() => _PostImagePageState();
}

class _PostImagePageState extends State<PostImagePage> {
  bool showOverlay = true;

  _openInReddit() =>
      launch('https://www.reddit.com${widget.post.data['permalink']}');

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (mounted) setState(() => showOverlay = !showOverlay);
        widget.onTap(showOverlay);
      },
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          ClipRect(
            child: PhotoView(
              imageProvider: NetworkImage(widget.post.url.toString()),
              heroTag: widget.post.url.toString(),
            ),
          ),
          showOverlay
              ? Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    color: Colors.black45,
                    padding: EdgeInsets.all(16.0).copyWith(top: 0.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            FlatButton.icon(
                              padding: EdgeInsets.symmetric(horizontal: 8.0),
                              textColor: Colors.white70,
                              onPressed: () {},
                              icon: Icon(Icons.share, size: 16.0),
                              label: Text('Share with firm'),
                            ),
                            FlatButton.icon(
                              padding: EdgeInsets.symmetric(horizontal: 8.0),
                              textColor: Colors.white,
                              onPressed: _openInReddit,
                              icon: Icon(Icons.launch, size: 16.0),
                              label: Text('Open in Reddit'),
                            ),
                          ],
                        ),
                        SizedBox(height: 4.0),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Icon(
                                  Icons.arrow_upward,
                                  color: Colors.white,
                                  size: 16.0,
                                ),
                                SizedBox(width: 4.0),
                                Text(
                                  '${widget.post.upvotes - widget.post.downvotes}',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                SizedBox(width: 8.0),
                                Icon(
                                  Icons.comment,
                                  color: Colors.white,
                                  size: 16.0,
                                ),
                                SizedBox(width: 4.0),
                                Text(
                                  '${widget.post.numComments}',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                SizedBox(width: 8.0),
                                Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.access_time,
                                      color: Colors.white,
                                      size: 14.0,
                                    ),
                                    SizedBox(width: 4.0),
                                    Text(
                                      timeago.format(widget.post.createdUtc),
                                      style: TextStyle(
                                        fontSize: 12.0,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            Expanded(
                              child: GestureDetector(
                                onTap: () => Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (_) => StocksPage(
                                              username: widget.post.author,
                                            ),
                                      ),
                                    ),
                                child: AutoSizeText(
                                  '${widget.post.author}',
                                  maxLines: 1,
                                  textAlign: TextAlign.end,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                )
              : Container(),
          showOverlay
              ? Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    color: Colors.black45,
                    padding: const EdgeInsets.all(8.0).copyWith(top: 24.0),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            '< Meme statistics',
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                          Text(
                            'Invest >',
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              : Container(),
        ],
      ),
    );
  }
}
