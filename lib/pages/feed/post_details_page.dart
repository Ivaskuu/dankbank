import 'package:async/async.dart';
import 'package:dankbank/managers/api_manager.dart';
import 'package:dankbank/managers/reddit_manager.dart';
import 'package:dankbank/models/investment.dart';
import 'package:draw/draw.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

enum ChartRange { M5, M30, H1, H2, D1 }

class PostDetailsPage extends StatefulWidget {
  final Submission post;
  const PostDetailsPage({Key key, this.post}) : super(key: key);

  @override
  _PostDetailsPageState createState() => _PostDetailsPageState();
}

class _PostDetailsPageState extends State<PostDetailsPage> {
  AsyncMemoizer invMem = AsyncMemoizer();
  ChartRange chartRange;

  _chartTimeToDateTime(ChartRange range) {
    DateTime minTime;

    if (range == ChartRange.M5)
      minTime = DateTime.now().subtract(Duration(minutes: 5));
    else if (range == ChartRange.M30)
      minTime = DateTime.now().subtract(Duration(minutes: 30));
    else if (range == ChartRange.H1)
      minTime = DateTime.now().subtract(Duration(hours: 1));
    else if (range == ChartRange.H2)
      minTime = DateTime.now().subtract(Duration(hours: 2));
    else if (range == ChartRange.D1)
      minTime = DateTime.now().subtract(Duration(days: 1));

    return minTime;
  }

  Widget _buildUpvotesChart(charts.Color blue, Row chartTimeRow) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Material(
        elevation: 6.0,
        shadowColor: Colors.black38,
        color: Colors.white,
        borderRadius: BorderRadius.circular(8.0),
        clipBehavior: Clip.antiAlias,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: SizedBox(
            height: 300.0,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Text(
                  'Upvotes chart',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    // fontSize: 32.0,
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FutureBuilder(
                      future: invMem.runOnce(
                        () => ApiManager.getMemeInvestmentsV2(
                              widget.post.id,
                              minTime: _chartTimeToDateTime(chartRange),
                            ),
                      ),
                      builder: (context, snapshot) {
                        if (snapshot.connectionState == ConnectionState.done &&
                            snapshot.hasData) {
                          print((snapshot.data as List).length);

                          if ((snapshot.data as List).length >= 2) {
                            return charts.TimeSeriesChart(
                              [
                                charts.Series<Investment, DateTime>(
                                  id: 'Upvotes',
                                  colorFn: (_, __) => blue,
                                  domainFn: (Investment inv, _) =>
                                      DateTime.fromMillisecondsSinceEpoch(
                                          inv.time * 1000),
                                  measureFn: (Investment inv, _) =>
                                      inv.initUpvotes,
                                  data: snapshot.data,
                                ),
                              ],
                              animate: false,
                            );
                          } else
                            return Center(
                              child: Text('Not enough data'),
                            );
                        } else
                          return Center(
                            child: CircularProgressIndicator(),
                          );
                      },
                    ),
                  ),
                ),
                SizedBox(height: 16.0),
                chartTimeRow,
              ],
            ),
          ),
        ),
      ),
    );
  }

  /* Widget _buildInvDistribution(charts.Color blue) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Material(
        elevation: 6.0,
        shadowColor: Colors.black38,
        color: Colors.white,
        borderRadius: BorderRadius.circular(8.0),
        clipBehavior: Clip.antiAlias,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: SizedBox(
            height: 300.0,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Text(
                  'Investments distribution',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    // fontSize: 32.0,
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FutureBuilder(
                      future: invMem.runOnce(
                        () => ApiManager.getMemeInvestmentsV2(
                              widget.post.id,
                              minTime: _chartTimeToDateTime(chartRange),
                            ),
                      ),
                      builder: (context, snapshot) {
                        if (snapshot.connectionState == ConnectionState.done &&
                            snapshot.hasData) {
                          print((snapshot.data as List).length);

                          if ((snapshot.data as List).length >= 2) {
                            return charts.TimeSeriesChart(
                              [
                                charts.Series<Investment, DateTime>(
                                  id: 'Upvotes',
                                  colorFn: (_, __) => blue,
                                  domainFn: (Investment inv, _) =>
                                      DateTime.fromMillisecondsSinceEpoch(
                                          inv.time * 1000),
                                  measureFn: (Investment inv, _) =>
                                      inv.initUpvotes,
                                  data: snapshot.data,
                                ),
                              ],
                              animate: false,
                            );
                          } else
                            return Center(
                              child: Text('Not enough data'),
                            );
                        } else
                          return Center(
                            child: CircularProgressIndicator(),
                          );
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
 */
  @override
  Widget build(BuildContext context) {
    final blue = charts.Color(
      r: Theme.of(context).accentColor.red,
      g: Theme.of(context).accentColor.green,
      b: Theme.of(context).accentColor.blue,
    );

    final chartTimeRow = Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        GestureDetector(
          onTap: () {
            invMem = AsyncMemoizer();
            setState(() => chartRange = ChartRange.M5);
          },
          child: Text(
            '5m',
            style: TextStyle(
              fontWeight: chartRange == ChartRange.M5
                  ? FontWeight.bold
                  : FontWeight.w400,
              color: chartRange == ChartRange.M5
                  ? Theme.of(context).accentColor
                  : Colors.black,
            ),
          ),
        ),
        GestureDetector(
          onTap: () {
            invMem = AsyncMemoizer();
            setState(() => chartRange = ChartRange.M30);
          },
          child: Text(
            '30m',
            style: TextStyle(
              fontWeight: chartRange == ChartRange.M30
                  ? FontWeight.bold
                  : FontWeight.w400,
              color: chartRange == ChartRange.M30
                  ? Theme.of(context).accentColor
                  : Colors.black,
            ),
          ),
        ),
        GestureDetector(
          onTap: () {
            invMem = AsyncMemoizer();
            setState(() => chartRange = ChartRange.H1);
          },
          child: Text(
            '1h',
            style: TextStyle(
              fontWeight: chartRange == ChartRange.H1
                  ? FontWeight.bold
                  : FontWeight.w400,
              color: chartRange == ChartRange.H1
                  ? Theme.of(context).accentColor
                  : Colors.black,
            ),
          ),
        ),
        GestureDetector(
          onTap: () {
            invMem = AsyncMemoizer();
            setState(() => chartRange = ChartRange.H2);
          },
          child: Text(
            '2h',
            style: TextStyle(
              fontWeight: chartRange == ChartRange.H2
                  ? FontWeight.bold
                  : FontWeight.w400,
              color: chartRange == ChartRange.H2
                  ? Theme.of(context).accentColor
                  : Colors.black,
            ),
          ),
        ),
        GestureDetector(
          onTap: () {
            invMem = AsyncMemoizer();
            setState(() => chartRange = ChartRange.D1);
          },
          child: Text(
            '1d',
            style: TextStyle(
              fontWeight: chartRange == ChartRange.D1
                  ? FontWeight.bold
                  : FontWeight.w400,
              color: chartRange == ChartRange.D1
                  ? Theme.of(context).accentColor
                  : Colors.black,
            ),
          ),
        ),
        GestureDetector(
          onTap: () {
            invMem = AsyncMemoizer();
            setState(() => chartRange = null);
          },
          child: Text(
            'ALL',
            style: TextStyle(
              fontWeight:
                  chartRange == null ? FontWeight.bold : FontWeight.w400,
              color: chartRange == null
                  ? Theme.of(context).accentColor
                  : Colors.black,
            ),
          ),
        ),
      ],
    );

    final investButton = Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Material(
        elevation: 6.0,
        clipBehavior: Clip.antiAlias,
        borderRadius: BorderRadius.circular(4.0),
        color: Theme.of(context).accentColor,
        child: InkWell(
          // onTap: () => Navigator.push(context,
          //     MaterialPageRoute(builder: (_) => InvestPage(post: widget.post))),
          child: Padding(
            padding: EdgeInsets.all(12.0),
            child: Text(
              'Invest',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ),
    );

    return ListView(
      children: <Widget>[
        SizedBox(height: 8.0),
        AppBar(
          automaticallyImplyLeading: false,
          elevation: 0.0,
          title: Text(
            'Meme Statistics',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 32.0,
            ),
          ),
        ),
        _buildUpvotesChart(blue, chartTimeRow),
      ],
    );
  }
}
