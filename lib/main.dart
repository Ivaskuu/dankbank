import 'package:dankbank/managers/auth_manager.dart';
import 'package:dankbank/pages/splash_screen.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    AuthManager.instance;

    return MaterialApp(
      title: 'DankBank',
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Colors.white,
        accentColor: Color(0xff0322D6),
        scaffoldBackgroundColor: Colors.white,
        fontFamily: 'GoogleSans',

        // Dark mode ?
        // accentColor: Color(0xffF24285),
        // scaffoldBackgroundColor: Color(0xFF1D282E),
      ),
      home: SplashScreen(),
      // navigatorObservers: [
      //   AuthManager.instance.getAnalyticsObserver(),
      // ],
    );
  }
}
