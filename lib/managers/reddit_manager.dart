import 'package:dankbank/managers/auth_manager.dart';
import 'package:dankbank/pages/onboarding/reddit_login_browser.dart';
import 'package:dankbank/pages/splash_screen.dart';
import 'package:draw/draw.dart';
import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

class RedditManager {
  static RedditManager _instance;

  static RedditManager get instance {
    if (_instance == null) _instance = RedditManager();
    return _instance;
  }

  static const String CLIENT_ID = 'IImII0CdoQEvRQ';
  Reddit reddit;

  Future init() async {
    String credentials =
        AuthManager.instance.prefs.getString('credentialsJson');

    if (credentials != null) {
      await initWithCredentials(credentials);
    } else {
      await initNoCredentials();
    }
  }

  Future initNoCredentials() async {
    reddit = await Reddit.createUntrustedReadOnlyInstance(
      clientId: CLIENT_ID,
      userAgent: 'dankbank 0.4.1 by /u/ivaskuu',
      deviceId: Uuid().v4(),
    );
  }

  Future initWithCredentials(String credentialsJson) async {
    reddit = await Reddit.restoreAuthenticatedInstance(
      credentialsJson,
      clientId: CLIENT_ID,
      clientSecret: '',
      redirectUri: Uri.parse(r'https://www.reddit.com/r/MemeEconomy'),
      userAgent: 'android:com.skuu.dankbank 0.4.1 by /u/ivaskuu',
    );
  }

  Future loginUser(BuildContext context) async {
    print('login user');

    reddit = Reddit.createWebFlowInstance(
      clientId: CLIENT_ID,
      clientSecret: '',
      redirectUri: Uri.parse(r'https://www.reddit.com/r/MemeEconomy'),
      userAgent: 'android:com.skuu.dankbank 0.4.1 by /u/ivaskuu',
    );

    WebAuthenticator auth = reddit.auth;
    final url = auth.url([
      'vote',
      'identity',
      'read',
      'submit',
      'edit',
    ], 'RaNdOmStRinG', compactLogin: true);
    print(url);

    RedditLoginBrowser browser;
    browser = RedditLoginBrowser((String uri) async {
      print('New url: $uri');
      if (uri.startsWith('https://www.reddit.com/r/MemeEconomy')) {
        print('Ladies and gentleman, we gottem');

        await browser.close();

        final code = uri
            .split('https://www.reddit.com/r/MemeEconomy')[1]
            .split('code=')[1];

        print('\n\n$code\n\n');
        _authorizeUser(code, auth, context);
      }
    });

    browser.open(
      url: url.toString(),
      options: {'clearCache': true, 'hideUrlBar': true},
    );
  }

  Future _authorizeUser(
      String code, WebAuthenticator auth, BuildContext context) async {
    try {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => AlertDialog(
              content: Row(
                children: <Widget>[
                  CircularProgressIndicator(),
                  SizedBox(width: 8.0),
                  Expanded(child: Text('Please wait...')),
                ],
              ),
            ),
      );

      await auth.authorize(code);
      print('authorized');

      AuthManager.instance.prefs
          .setString('credentialsJson', reddit.auth.credentials.toJson());

      final me = await reddit.user.me();
      AuthManager.instance.setUsername(me.displayName);
      Navigator.pop(context);

      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) => Dialog(
              child: Material(
                borderRadius: BorderRadius.circular(8.0),
                clipBehavior: Clip.antiAlias,
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Text(
                        'Do you want to enable notifications?',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                          fontSize: 20.0,
                        ),
                      ),
                      SizedBox(height: 8.0),
                      Text(
                        'You will receive a notification when your investments matures.',
                      ),
                      SizedBox(height: 24.0),
                      Material(
                        elevation: 6.0,
                        clipBehavior: Clip.antiAlias,
                        borderRadius: BorderRadius.circular(4.0),
                        color: Theme.of(context).accentColor,
                        child: InkWell(
                          onTap: () {
                            AuthManager.instance.analytics.logEvent(
                              name: 'enable_notifications',
                            );

                            AuthManager.instance.setNotifEnable(true);
                            Navigator.pop(context);

                            Navigator.popUntil(
                                context, (route) => route != null);
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (_) => SplashScreen()));
                          },
                          child: Padding(
                            padding: EdgeInsets.all(12.0),
                            child: Text(
                              'Sure',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 8.0),
                      Material(
                        clipBehavior: Clip.antiAlias,
                        borderRadius: BorderRadius.circular(4.0),
                        color: Colors.black12,
                        child: InkWell(
                          onTap: () {
                            AuthManager.instance.analytics.logEvent(
                              name: 'disable_notifications',
                            );

                            AuthManager.instance.setNotifEnable(false);
                            Navigator.pop(context);

                            Navigator.popUntil(
                                context, (route) => route != null);
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (_) => SplashScreen()));
                          },
                          child: Padding(
                            padding: EdgeInsets.all(12.0),
                            child: Text(
                              'No, thanks',
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.black),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              backgroundColor: Colors.transparent,
            ),
      );
    } catch (ex) {
      throw Exception('Error: $ex');
    }
  }

  Future getSubmission(String id) async {
    if (reddit == null) await init();

    final submission = await reddit.submission(id: id).populate();
    return submission;
  }

  Future<double> parseFirmProfitBotComment(String id, String firmName) async {
    if (reddit == null) await init();

    final comment = await reddit.comment(id: id).populate();
    final body = comment.body;

    if (body.toLowerCase().contains(firmName.toLowerCase())) {
      final double firmTax = double.tryParse(body
          .split('MemeCoins were sent to the firm')[0]
          .split('.')
          .last
          .replaceAll(RegExp('[^\\d.]'), '')
          .trim());

      return firmTax ?? 0;
    }

    return 0;
  }

  Future votePost(bool voteStatus, {String id, Submission submission}) async {
    if (reddit == null) await init();

    Submission s;

    if (id != null)
      s = await reddit.submission(id: id).populate();
    else if (submission != null)
      s = submission;
    else
      throw Exception(
          'Yo man why do you gotta b like dis? submission not provided');

    if (voteStatus == null)
      await s.clearVote();
    else if (voteStatus)
      await s.upvote();
    else
      await s.downvote();
  }

  Future<Comment> postReply(String comment, String commentId) async {
    if (reddit == null) await init();

    Comment comm = await reddit.comment(id: commentId).populate();
    Comment reply = await comm.reply(comment);

    return reply;
  }

  Future<String> findBotCommentId(String postId) async {
    Submission post = await reddit.submission(id: postId).populate();

    final bot = 'MemeInvestor_bot';
    for (var i = 0; i < post.comments.length; i++) {
      if (post.comments.comments[i].runtimeType == Comment) {
        final comment = post.comments.comments[i] as Comment;
        if (comment.author == bot) {
          return comment.id;
        }
      }
    }

    return null;
  }

  Stream getFeed({sort: Sort.hot}) {
    if (reddit == null) {
      init();
      return null;
    } else {
      final subreddit = reddit.subreddit('MemeEconomy');
      Stream postsStream;

      if (sort == Sort.hot)
        postsStream = subreddit.hot();
      else if (sort == Sort.newest)
        postsStream = subreddit.newest();
      else
        postsStream = subreddit.top();

      return postsStream;
    }
  }

  Future<String> getBotCommentId() async {
    await RedditManager.instance.init();
    Submission post =
        await RedditManager.instance.getFeed(sort: Sort.newest).first;

    String botCommId = await RedditManager.instance.findBotCommentId(post.id);
    return botCommId;
  }

  Future execCommand(String command) async =>
      RedditManager.instance.postReply(command, await getBotCommentId());

  Future<Map> getMemeStatBot({String id, Submission submission}) async {
    final String bot = 'Meme_Stat_Bot';

    double loss;
    double breakEven;
    double profit;

    Submission post;
    if (id != null)
      post = await reddit.submission(id: id).populate();
    else
      post = submission;

    await post.refreshComments(sort: CommentSortType.newest);
    print('refreshed comments');

    for (var i = 0; i < post.comments.length; i++) {
      if (post.comments[i].runtimeType != MoreComments) {
        final comment = post.comments.comments[i] as Comment;

        if (comment.author == bot) {
          String body = comment.body.replaceAll('\n', '');

          var values = body.split('  ');
          loss = double.tryParse(values[1].split('%')[0]);
          breakEven = double.tryParse(values[2].split('%')[0]);
          profit = double.tryParse(values[3].split('%')[0]);

          break;
        }
      }
    }

    return {'loss': loss, 'breakEven': breakEven, 'profit': profit};
  }

  Future editCommentPromo(Comment comment) async {
    final c =
        r'> Invested with [DankBank for Android](https://play.google.com/store/apps/details?id=com.skuu.dankbank). The easiset way to invest in memes.';
    
    try {
      print('Editing comment');
      await comment.edit('${comment.body}\n\n$c');
    } catch (ex) {
      print(
          'Couldn\'t edit comment. User probably doesn\'t have edit permission: $ex');
    }
  }
}
