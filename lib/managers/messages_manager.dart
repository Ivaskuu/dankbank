import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dankbank/managers/api_manager.dart';
import 'package:dankbank/managers/auth_manager.dart';
import 'package:dankbank/managers/fcm_manager.dart';
import 'package:dankbank/models/message.dart';

class MessagesManager {
  static MessagesManager _instance;

  static MessagesManager get instance {
    if (_instance == null) _instance = MessagesManager();
    return _instance;
  }

  // These tags are for both logging and accessing Firestore's collections
  static const String TAG = 'MESSAGES_MANAGER';
  static const String FIRMS_TAG = 'firms';
  static const String MESSAGES_TAG = 'messages';

  static const Duration MAX_TIME_DELETE_MESSAGE = Duration(minutes: 10);
  static const Duration MAX_TIME_EDIT_MESSAGE = Duration(hours: 1);

  final Firestore _store = Firestore.instance;

  // CollectionReference get collection => _store.collection(COLLECTION_TAG);

  CollectionReference get firmsCollection => _store.collection(FIRMS_TAG);

  Query getMessages(String firmName, {int limit: 50}) =>
      _store.collection('$FIRMS_TAG/$firmName/$MESSAGES_TAG').limit(limit);

  Future<bool> checkFirmChatExists(String firmName) async {
    final doc = await _store.document('$FIRMS_TAG/$firmName').get();
    return doc.exists;
  }

  Future<String> getLastMessageId(String firmName) async {
    final query = await _store
        .collection('$FIRMS_TAG/$firmName/$MESSAGES_TAG')
        .orderBy('timestamp', descending: true)
        .limit(1)
        .getDocuments();
    final doc = query.documents.first;
    final msg = Message.fromDocumentSnapshot(doc);

    print(msg.content);

    return msg.id;
  }

  Future<bool> checkNewMsgs() async {
    if (AuthManager.instance.localUser == null ||
        AuthManager.instance.localUser.firm == null) return null;

    final firm = await ApiManager.getFirm(AuthManager.instance.localUser.firm);
    final lastId = await getLastMessageId(firm.name);
    final lastSavedId = AuthManager.instance.prefs.getString('last_msg_id');

    return lastId != lastSavedId;
  }

  Future<Message> sendMessage(
    Message message,
    String firmName,
    int firmId,
  ) async {
    String error;

    final errorHandler = (exception, stacktrace) {
      print(
          '[$TAG] Couldn\'t send new Message, error: $exception\n$stacktrace');
      error = exception.toString();
    };

    bool chatExists =
        await MessagesManager.instance.checkFirmChatExists(firmName);

    if (!chatExists) {
      print('chat will be created');

      final chatRef = firmsCollection.document(firmName);
      await _store.runTransaction((transaction) async {
        await transaction.set(chatRef, {}).catchError(errorHandler);
        print('Chat created and added');
      }).catchError(errorHandler);
    }

    final msgRef =
        firmsCollection.document(firmName).collection(MESSAGES_TAG).document();
    message.id = msgRef.documentID;

    await _store.runTransaction((transaction) async {
      final msgFreshSnap = await msgRef.get();
      if (!msgFreshSnap.exists)
        await transaction.set(msgRef, message.toMap()).catchError(errorHandler);
    }).catchError(errorHandler);

    FcmManager.sendFirmChatNotification(message.from, message.content, firmId);
    return message;
  }

  /* Future<Message> sendImageMessage(Message message, File image) async {
    String error;

    final errorHandler = (exception, stacktrace) {
      print('[$TAG] Couldn\'t send new Message, error: $exception');
      error = exception.toString();
    };

    DocumentReference reference;
    if (message.id == null) {
      reference = chatsCollection
          .document(message.chatId)
          .collection(MESSAGES_TAG)
          .document();
      message.id = reference.documentID;
    } else {
      reference = chatsCollection
          .document(message.chatId)
          .collection(MESSAGES_TAG)
          .document(message.id);
    }

    String imageUrl =
        (await StorageManager.instance.uploadImageMessage(message, image)).data;
    message.content = imageUrl;

    await _store.runTransaction((transaction) async {
      final freshSnap = await reference.get();
      if (!freshSnap.exists)
        await transaction
            .set(reference, message.toMap())
            .catchError(errorHandler);
    }).catchError(errorHandler);

    return message;
  }
 */
  Future<Message> deleteMessage(Message message, String firmName) async {
    String error;

    final errorHandler = (exception, stacktrace) {
      print('$TAG Couldn\'t delete the Message, error: $exception');
      error = exception.toString();
    };

    if (AuthManager.instance.localUser.username == message.from) {
      final reference = firmsCollection
          .document(firmName)
          .collection(MESSAGES_TAG)
          .document(message.id);

      await _store.runTransaction((transaction) async {
        final freshSnap = await reference.get();
        if (freshSnap.exists)
          await transaction.delete(reference).catchError(errorHandler);
      }).catchError(errorHandler);

      return message;
    }

    return null;
  }
}
