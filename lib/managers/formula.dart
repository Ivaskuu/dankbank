import 'dart:math';

class Formula {
  static double calculate(double amount, int initUps, int finalUps) {
    // Inverse
    // 1 = (a / (1 + exp(-(s * (d - m))))) - 1

    // Treat anything below 0 upvotes as 0 upvotes
    if (initUps < 0) initUps = 0;
    if (finalUps < 0) finalUps = 0;

    // Compute gain
    var delta = finalUps - initUps;

    // Treat negative gain as no gain
    if (delta < 0) delta = 0;

    // Compute the maximum of the sigmoid
    var sig_max = sigmoid_max(initUps);

    // Compute the midpoint of the sigmoid
    var sig_mp = sigmoid_midpoint(initUps);

    // Compute the steepness of the sigmoid
    var sig_stp = sigmoid_steepness(initUps);

    // Calculate return
    var factor = sigmoid(delta, sig_max, sig_mp, sig_stp);

    // Normalize between -1 and 1
    factor = factor - 1;

    // Adjust based on net worth
    factor = factor * amount_coefficient(amount);

    // Return investment amount multiplier (change + 1)
    return (factor + 1) * amount - amount;
  }

  static int breakEven(double amount, int initUps) {
    for (var i = initUps; i < initUps * 1000; i++) {
      double profit = calculate(amount, initUps, i);
      if (profit > 0) return i;
    }

    print('Couldn\'t find a break even point');
    return null;
  }

  static double sigmoid(x, maxvalue, midpoint, steepness) {
    var arg = -(steepness * (x - midpoint));
    var y = maxvalue / (1 + exp(arg));
    return y;
  }

  static double sigmoid_max(initUps) => 1.2 + 0.6 / ((initUps / 10) + 1);

  static double sigmoid_midpoint(initUps) {
    var sig_mp_0 = 10;
    var sig_mp_1 = 500;
    return linear_interpolate(initUps, 0, 25000, sig_mp_0, sig_mp_1);
  }

  static double sigmoid_steepness(initUps) => 0.06 / ((initUps / 100) + 1);

  static double linear_interpolate(x, x_0, x_1, y_0, y_1) {
    var m = (y_1 - y_0) / (x_1 - x_0);
    var c = y_0;
    var y = (m * x) + c;
    return y;
  }

  static double amount_coefficient(amount) => pow(amount, -0.155) * 6;
}
