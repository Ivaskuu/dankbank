import 'dart:async';

import 'package:dankbank/managers/api_manager.dart';
import 'package:dankbank/managers/user_manager.dart';
import 'package:dankbank/models/fb_user.dart';
import 'package:dankbank/models/user.dart' as my;
import 'package:draw/draw.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthManager {
  static AuthManager _instance;

  static AuthManager get instance {
    if (_instance == null) _instance = AuthManager();
    return _instance;
  }

  AuthManager() {
    initPrefs();
    initNotif();
  }

  SharedPreferences prefs;
  FlutterLocalNotificationsPlugin notif;
  FirebaseAnalytics analytics = FirebaseAnalytics();
  FirebaseAnalyticsObserver _analyticsObserver;

  bool hasAccount = false;

  String _username;
  my.User localUser;
  FbUser fbUser;

  List<String> _friends = [];
  Sort _sortType = Sort.newest;

  Future initPrefs() async {
    prefs = prefs ?? await SharedPreferences.getInstance();
    return prefs;
  }

  Future initNotif() async {
    final androidInit = AndroidInitializationSettings('@mipmap/ic_launcher');
    final initSettings = InitializationSettings(androidInit, null);

    notif = FlutterLocalNotificationsPlugin();
    notif.initialize(initSettings);
  }

  FirebaseAnalyticsObserver getAnalyticsObserver() {
    if (_analyticsObserver == null)
      _analyticsObserver = FirebaseAnalyticsObserver(analytics: analytics);

    return _analyticsObserver;
  }

  String getUsername() {
    _username = prefs.getString('username');

    if (fbUser == null)
      getUser()
          .then((u) async => fbUser = await UserManager.instance.getFbUser(u));

    return _username;
  }

  Future<my.User> getUser() async {
    if (localUser == null) localUser = await ApiManager.getUser(_username);
    return localUser;
  }

  setUsername(String username) => prefs.setString('username', username);

  List<String> getFriends() {
    _friends = prefs.getStringList('friends') ?? [];
    return _friends;
  }

  addFriend(String username) {
    _friends = getFriends()..add(username);
    prefs.setStringList('friends', _friends);
  }

  removeFriend(String username) {
    _friends = getFriends()..remove(username);
    prefs.setStringList('friends', _friends);
  }

  Sort getSortType() {
    _sortType = Sort.values[prefs.getInt('sortType') ?? 3];
    return _sortType;
  }

  setSortType(Sort sortType) {
    _sortType = sortType;
    prefs.setInt('sortType', sortType.index);
  }

  bool isNotifEnabled() {
    return prefs.getBool('notifEnabled');
  }

  setNotifEnable(bool state) {
    prefs.setBool('notifEnabled', state);
  }
}
