import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dankbank/managers/fcm_manager.dart';
import 'package:dankbank/models/fb_user.dart';
import 'package:dankbank/models/message.dart';
import 'package:dankbank/models/user.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class UserManager {
  static UserManager _instance;

  static UserManager get instance {
    if (_instance == null) _instance = UserManager();
    return _instance;
  }

  // These tags are for both logging and accessing Firestore's collections
  static const String USERS_TAG = 'users';
  CollectionReference get firmsCollection => _store.collection(USERS_TAG);

  final Firestore _store = Firestore.instance;

  FbUser localFbUser;

  Future<FbUser> getFbUser(User user) async {
    if (localFbUser != null) return localFbUser;

    final snapshot = await firmsCollection.document(user.username).get();
    if (snapshot.exists)
      localFbUser = FbUser.fromDocumentSnapshot(snapshot);
    else
      localFbUser = await createUser(user);

    subscribeFirmChat(user.firm);
    return localFbUser;
  }

  Future<bool> checkUserDownloadedApp(String name) async {
    final snapshot = await firmsCollection.document(name).get();
    return snapshot.exists;
  }

  Future<FbUser> createUser(User user) async {
    String token = await FirebaseMessaging().getToken();
    if (user.firm != null && user.firm != 0) {
      subscribeFirmChat(user.firm);
    }

    final fbUser = FbUser(name: user.username, fcmToken: token);

    final userRef = firmsCollection.document(user.username);
    await _store.runTransaction((transaction) async {
      await transaction
          .set(userRef, fbUser.toMap())
          .catchError((_) => print('Error creating new fbuser'));
      print('Chat created and added');
    }).catchError((_) => print('Error creating new fbuser'));

    return fbUser;
  }

  void subscribeFirmChat(int firmId) {
    print(firmId);
    print('subscirbe');

    if (firmId != null && firmId != 0) {
      FirebaseMessaging().subscribeToTopic('firm_$firmId');
    } else {
      unsubscribeFirmChat();
    }
  }

  void unsubscribeFirmChat() async {
    final Map subscriptions =
        await FcmManager.getSubscribedTopics(localFbUser.fcmToken);

    if (subscriptions != null)
      for (var topic in subscriptions.keys)
        if ((topic as String).startsWith('firm_'))
          FirebaseMessaging().unsubscribeFromTopic(topic);
  }
}
