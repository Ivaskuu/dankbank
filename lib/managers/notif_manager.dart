import 'dart:ui';

import 'package:dankbank/managers/auth_manager.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class NotifManager {
  static NotifManager _instance;

  static NotifManager get instance {
    if (_instance == null) _instance = NotifManager();
    return _instance;
  }

  FlutterLocalNotificationsPlugin notif;

  Future initNotif() async {
    final androidInit = AndroidInitializationSettings('notif_icon');
    final initSettings = InitializationSettings(androidInit, null);

    notif = FlutterLocalNotificationsPlugin();
    notif.initialize(initSettings);
  }

  Future scheduleNotif(DateTime time) async {
    if (AuthManager.instance.isNotifEnabled() != null &&
        AuthManager.instance.isNotifEnabled()) {
      if (notif == null) await initNotif();

      var android = AndroidNotificationDetails(
        'investment_matured',
        'Matured investments',
        'When an investment matures',
        importance: Importance.Max,
        priority: Priority.High,
        color: Color(0xff0322D6),
      );

      var platformChannelSpecifics = NotificationDetails(android, null);

      int id = time.millisecondsSinceEpoch ~/ 1000;
      await notif.cancel(id);

      await notif.schedule(
        id,
        'Your investment has matured',
        'Open the app to see how it went',
        time,
        platformChannelSpecifics,
      );

      print('Notification scheduled');
    }
  }
}
