import 'package:dankbank/managers/reddit_manager.dart';
import 'package:dankbank/managers/user_manager.dart';
import 'package:dankbank/models/firm.dart';
import 'package:dankbank/models/investment.dart';
import 'package:dankbank/models/user.dart';
import 'package:draw/draw.dart' as draw;
import 'package:http/http.dart' as http;
import 'dart:convert';

class ApiManager {
  static const _URL = r'https://meme.market/api';

  static Future<User> getUser(String user) async {
    final url = '$_URL/investor/$user';
    final response = await http.get(url);

    if (response.statusCode == 200) {
      try {
        final data = json.decode(response.body);
        final user = User.fromMap(data);

        return user;
      } catch (ex) {
        print('[API_MANAGER] There was an error getting user: $ex');
      }
    }
    return null;
  }

  static Future<List<Investment>> getInvestments(String user,
      {int page: 0, int ith: 0}) async {
    if (ith < 3) {
      final url =
          '$_URL/investor/$user/investments?per_page=15page=$page&from=&to=';
      final response = await http.get(url);

      if (response.statusCode == 200) {
        try {
          final List data = json.decode(response.body);
          return data.map((inv) => Investment.fromMap(inv)).toList();
        } catch (ex) {
          print(response.statusCode);
          print(response.reasonPhrase);
          print(response.request);
          print(response.body);
          print(response.contentLength);
          print(response.bodyBytes);

          print(
              '[API_MANAGER] There was an error getting user investments: $ex');
          return await getInvestments(user, page: page, ith: ++ith);
        }
      }
    }
    return null;
  }

  static Future<Firm> getFirm(int id) async {
    final url = '$_URL/firm/$id';
    final response = await http.get(url);

    if (response.statusCode == 200) {
      try {
        final data = json.decode(response.body);
        return Firm.fromMap(data);
      } catch (ex) {
        print(response.statusCode);
        print(response.reasonPhrase);
        print(response.request);
        print(response.body);
        print('[API_MANAGER] There was an error getting the firm: $ex');
      }
    }

    print(response.statusCode);
    print(response.reasonPhrase);
    print(response.request);
    print(response.body);
    return null;
  }

  static Future<List<User>> getFirmUsers(int id, {int page: 0}) async {
    final url = '$_URL/firm/$id/members';
    final response = await http.get(url);

    if (response.statusCode == 200) {
      try {
        final List data = json.decode(response.body);
        return data.map((inv) => User.fromMap(inv)).toList();
      } catch (ex) {
        print(response.statusCode);
        print(response.reasonPhrase);
        print(response.request);
        print(response.body);
        print('[API_MANAGER] There was an error getting firm users: $ex');
      }
    }

    print(response.statusCode);
    print(response.reasonPhrase);
    print(response.request);
    print(response.body);
    return null;
  }

  static Future<List<Firm>> getFirmsList() async {
    final url = '$_URL/firms/top';
    List<Firm> firms = [];
    int page = 0;

    do {
      final response = await http.get('$url?page=$page');

      if (response.statusCode == 200) {
        try {
          final List data = json.decode(response.body);
          firms.addAll(data.map((inv) => Firm.fromMap(inv)));
        } catch (ex) {
          print(
              '[API_MANAGER] There was an error getting post investments: $ex');
          return null;
        }
      } else {
        print(response.statusCode);
        print(response.reasonPhrase);
        print(response.body);

        return null;
      }

      page++;
    } while (firms.length % 100 == 0 && page < 10);

    return firms;
  }

  static Future<int> getFirmRank(String firmId) async {
    final List firms = await getFirmsList();
    return firms.indexOf(firms.firstWhere((firm) => firm.id == firmId)) + 1;
  }

  static Future<List<Investment>> getMemeInvestmentsV2(String postId,
      {DateTime minTime}) async {
    final url = '$_URL/investments/post/$postId';
    List<Investment> investments = [];
    int page = 0;

    do {
      final response = await http.get('$url?page=$page');

      if (response.statusCode == 200) {
        try {
          final List data = json.decode(response.body);
          investments.addAll(
            data.map((inv) => Investment.fromMap(inv)).where(
                  (inv) => minTime != null
                      ? DateTime.fromMillisecondsSinceEpoch(inv.time * 1000)
                          .isAfter(minTime)
                      : true,
                ),
          );
        } catch (ex) {
          print(
              '[API_MANAGER] There was an error getting post investments: $ex');
          return null;
        }
      } else {
        print(response.statusCode);
        print(response.reasonPhrase);
        print(response.body);

        return null;
      }

      page++;
    } while (investments.length % 100 == 0 && page < 10); // Continue

    investments.sort((a, b) => a.time.compareTo(b.time));
    return investments;
  }

  static Future initUser() async {
    await RedditManager.instance.init();
    draw.Submission post =
        await RedditManager.instance.getFeed(sort: draw.Sort.newest).first;

    String botCommId = await RedditManager.instance.findBotCommentId(post.id);
    await RedditManager.instance.postReply('!balance', botCommId);
    await Future.delayed(Duration(seconds: 10));
  }
}
