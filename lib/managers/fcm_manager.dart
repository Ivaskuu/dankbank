import 'dart:convert';
import 'package:http/http.dart' as http;

class FcmManager {
  static const FCM_KEY =
      'AAAA1xtZ9Ww:APA91bEHMiDf7k5jMnGDZJLRP3n6dXcHuWI9HKVuKkVX5s1Xxlb9K523mMtbPbRo0Mxzo8C-hFbhoc440s1W2CfUFP-MSed6e_EtAj3IhLw47pGCBH9wOdDnCcs2FyNPj9R_WY8fksOc';
  static const String _URL = r'https://fcm.googleapis.com/fcm/send';

  static Future sendFirmChatNotification(
      String from, String body, int firmId) async {
    final jsonBody = json.encode({
      "to": "/topics/firm_$firmId",
      "notification": {
        "tag": "firm_${firmId}_chat",
        "title": from,
        "body": body,
        "icon": "notif_icon",
        "sound": "default",
        "color": "#0322D6",
        "click_action": "FLUTTER_NOTIFICATION_CLICK"
      }
    });

    final response = await http.post(
      _URL,
      body: jsonBody,
      headers: {
        "Authorization": "key=" + FCM_KEY,
        "Content-Type": "application/json",
      },
    ).timeout(Duration(seconds: 60));

    final result = json.decode(response.body);
    print(result);
  }

  static Future getSubscribedTopics(String token) async {
    String url = "https://iid.googleapis.com/iid/info/$token?details=true";

    final response = await http.post(
      url,
      headers: {"Authorization": "key=" + FCM_KEY},
    ).timeout(Duration(seconds: 60));

    final Map result = json.decode(response.body);
    if(result.containsKey('rel')) {
      return result['rel']['topics'];
    }
    return null;
  }
}
