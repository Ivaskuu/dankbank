import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:meta/meta.dart';

class FbUser {
  final String name;
  String fcmToken;

  FbUser({@required this.name, @required this.fcmToken});

  factory FbUser.fromDocumentSnapshot(DocumentSnapshot snapshot) {
    if (snapshot == null || snapshot.data.isEmpty) return null;

    final fbUser = FbUser(
      name: snapshot.documentID,
      fcmToken: snapshot.data['fcmToken'],
    );

    return fbUser;
  }

  Map<String, dynamic> toMap() => {
        'name': this.name,
        'fcmToken': this.fcmToken,
      };
}
