class Firm {
  final String id;
  final String name;
  final double balance;
  final int size;
  final int execs;
  final int tax;
  final int rank;
  final bool private;
  final double lastPayout;

  Firm({
    this.id,
    this.name,
    this.balance,
    this.size,
    this.execs,
    this.tax,
    this.rank,
    this.private,
    this.lastPayout,
  });

  factory Firm.fromMap(Map data) {
    final firm = Firm(
      id: data['id'].toString(),
      name: data['name'],
      balance: (data['balance'] ?? 0.0) * 1.0,
      size: data['size'] ?? 0,
      execs: data['broke'] ?? 0,
      tax: data['tax'] ?? 0,
      rank: data['rank'] ?? 0,
      private: data['private'],
      lastPayout: (data['lastPayout'] ?? 0.0) * 1.0,
    );
    return firm;
  }
}
