class Investment {
  final String id;
  final String postId;
  final int initUpvotes;
  final String commentId;
  final String username;
  final double amount;
  final int time;
  final bool done;
  final String responseId;
  final int finalUpvotes;
  final bool success;
  final double profit;

  Investment({
    this.id,
    this.postId,
    this.initUpvotes,
    this.commentId,
    this.username,
    this.amount,
    this.time,
    this.done,
    this.responseId,
    this.finalUpvotes,
    this.success,
    this.profit,
  });

  factory Investment.fromMap(Map data) {
    final investment = Investment(
      id: data['id'].toString(),
      postId: data['post'],
      initUpvotes: data['upvotes'],
      commentId: data['comment'],
      username: data['name'],
      amount: (data['amount'] ?? 0.0) * 1.0,
      time: data['time'],
      done: data['done'],
      responseId: data['response'],
      finalUpvotes: data['final_upvotes'],
      success: data['success'],
      profit: (data['profit'] ?? 0.0) * 1.0,
    );

    return investment;
  }
}
