import 'package:cloud_firestore/cloud_firestore.dart'
    show DocumentSnapshot, Timestamp;

class Message {
  static const String TAG = 'MESSAGE';

  String id;
  final String from;
  final int type; // 0: text, 1: image
  String content;
  Timestamp timestamp;

  Message({
    this.id,
    this.from,
    this.type: 0,
    this.content,
    this.timestamp,
  }) {
    timestamp = timestamp ?? Timestamp.now();
  }

  factory Message.fromDocumentSnapshot(DocumentSnapshot snapshot) {
    if (snapshot == null || !snapshot.exists || snapshot.data.isEmpty) {
      print('Message is null/not exists/is empty');
      return null;
    }
    try {
      return Message.fromMap(snapshot.data);
    } catch (error) {
      print('[$TAG] Couldn\'t build Message object, error: $error');
      return null;
    }
  }

  factory Message.fromMap(Map data) {
    try {
      final message = Message(
        id: data['id'],
        content: data['content'],
        from: data['from'],
        timestamp: data['timestamp'],
        type: data['type'],
      );

      return message;
    } catch (error) {
      print('[$TAG] Couldn\'t build Message object, error: $error');
      return null;
    }
  }

  Map<String, dynamic> toMap() => {
        'id': this.id,
        'content': this.content,
        'from': this.from,
        'timestamp': this.timestamp,
        'type': this.type,
      };

  static String dateTimeHourMinutes(DateTime time) {
    final now = DateTime.now();

    if (now.day == time.day &&
        now.month == time.month &&
        now.year == time.year) {
      // The same day

      return '${time.hour.toString().padLeft(2, '0')}:${time.minute.toString().padLeft(2, '0')}';
    } else
      return '${time.month.toString().padLeft(2, '0')}/${time.day.toString().padLeft(2, '0')} - ${time.hour.toString().padLeft(2, '0')}:${time.minute.toString().padLeft(2, '0')}';
  }
}
