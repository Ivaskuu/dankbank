enum FirmRole { Floor_Trader, Associate, Executive, CFO, COO, CEO }

class User {
  final String id;
  final String username;
  final double balance;
  final int transactionsNum;
  final int broke;
  // final List<dynamic> badges;
  final double networth;

  int firm;
  FirmRole firmRole;

  User({
    this.id,
    this.username,
    this.balance,
    this.transactionsNum,
    this.broke,
    // this.badges,
    this.networth,
    this.firm,
    this.firmRole,
  });

  factory User.fromMap(Map data) {
    final user = User(
      id: data['id'].toString(),
      username: data['name'],
      balance: (data['balance'] ?? 0.0) * 1.0,
      transactionsNum: data['completed'] ?? 0,
      broke: data['broke'] ?? 0,
      // badges: data['badges'],
      networth: (data['networth'] ?? 0.0) * 1.0,
      firm: data['firm'] == 0 ? null : data['firm'],
      firmRole: parseFirmRole(data['firm_role']),
    );
    return user;
  }

  static FirmRole parseFirmRole(String role) {
    if (role == '' || role == 'Floor Trader')
      return FirmRole.Floor_Trader;
    else if (role == 'assoc')
      return FirmRole.Associate;
    else if (role == 'exec')
      return FirmRole.Executive;
    else if (role == 'cfo')
      return FirmRole.CFO;
    else if (role == 'coo')
      return FirmRole.COO;
    else
      return FirmRole.CEO;
  }

  static bool roleCompare(FirmRole role1, FirmRole role2, {bool equal: true}) =>
      equal ? role1.index >= role2.index : role1.index > role2.index;
}
